{-# OPTIONS_GHC -fno-warn-type-defaults #-}
{-# LANGUAGE ImplicitParams #-}
{-# LANGUAGE ViewPatterns   #-}

import           Control.Monad
import           Data.Tree                    (drawForest)
import           Data.List                    (intercalate, sort)
import qualified Data.Map.Strict              as M
import           Data.Ratio
import qualified Data.Set                     as S
import           Data.Time
import qualified GUBS.Polynomial              as P
import           System.Environment
import           System.FilePath              (takeBaseName, (</>))
import           System.Directory             (createDirectoryIfMissing)
import           System.IO
import           System.IO.Unsafe             (unsafePerformIO)
import           System.Process               (readCreateProcess, shell)
import qualified System.Timeout               as Timeout
import qualified Text.PrettyPrint.ANSI.Leijen as PP

import           Data.PWhile.CostExpression
import qualified PWhile.InferEt               as InferEt
import           PWhile.Parser                (fromFile')
import           PWhile.DSL
import           PWhile.Util




-- A simple benchmarking tool.
--
-- The intended workflow is:
--
--   * make examples
--   * setup benchmark
--   * write result of benchmark to a file
--   * modify runime inference
--   * run benchmark against specified file
--
-- Problems are processed in order one by one as specified in the benchmark and
-- (de)serialised using Show and Read instances.
--

main :: IO ()
main = do
  let bnch = examples
  let ?to  = 60
  as <- getArgs
  case as of
    ("--run":_)      -> printBenchmark         bnch
    ("--write":fp:_) -> writeBenchmark      fp bnch
    ("--diff":fp:_)  -> runBenchmarkAgainst fp bnch
    ("--tex":_)      -> printBenchmarkTex      bnch
    ("--md":_)       -> printBenchmarkMd       bnch
    _                -> putStrLn usage
  where
    usage = unlines
      [ "--run         - run benchmark, print to stdout"
      , "--write <fp>  - write benchmark to fp"
      , "--diff <fp>   - test benchmark against fp"
      , "--md <fp>     - pretty print md format"
      , "--tex         - pretty print tex format" ]


type Name = String
type Time = String

data Benchmark = Benchmark Name [Problem]
data Problem   = Problem   Name Program
data Status    = Timeout | Failed | Success (CExp Rational) InferEt.Log
  deriving (Read, Show)

data Outcome   = Outcome Name Status Time
  deriving (Read, Show)


printBenchmark :: (?to :: Int) => Benchmark -> IO ()
printBenchmark (Benchmark _ ps) =
  forM_ ps $ \p -> do
    out <- run1 p
    putStrLn (withColorCode ppOutcome out)

writeBenchmark :: (?to :: Int) => FilePath -> Benchmark -> IO ()
writeBenchmark fp (Benchmark _ ps) =
  withFile fp WriteMode $ \hdl ->
    forM_ ps $ \p@(Problem pid _) -> do
      putStrLn ("processing ... " <> pid)
      out <- run1 p
      putStrLn (withColorCode (ppOutcome' "  ") out)
      hPrint hdl out

runBenchmarkAgainst :: (?to :: Int) => FilePath -> Benchmark -> IO ()
runBenchmarkAgainst fp (Benchmark _ ps) =
  withFile fp ReadMode $ \hdl ->
    forM_ ps $ \p -> do
      old <- read <$> hGetLine hdl
      new <- run1 p
      putDiff old new

red, green, yellow :: String -> String
red   xs  = "\ESC[31m" ++ xs ++ "\ESC[m"  -- worse
green xs  = "\ESC[32m" ++ xs ++ "\ESC[m"  -- better
yellow xs = "\ESC[33m" ++ xs ++ "\ESC[m"  -- equal / incomparable

fill :: Int -> String -> String
fill i xs = take i $ take i xs ++ repeat ' '

ppOutcome' :: String -> Outcome -> String
ppOutcome' pfx (Outcome pid st t) =
  pfx <> fill (50 - length pfx) pid <> ": " <> take 5 t <> "s : " <> ppStatus st

ppOutcome :: Outcome -> String
ppOutcome = ppOutcome' ""

withColorCode :: (Outcome -> String) -> Outcome -> String
withColorCode f o@(Outcome _ st _) = k st (f o) where
  k Success{} = green
  k _         = red

ppStatus :: Status -> String
ppStatus Timeout        = "[Timeout]"
ppStatus Failed         = "[Failed ]"
ppStatus (Success cexp _) = "[Success]" <> "    " <> renderPretty cexp

putDiff :: Outcome -> Outcome -> IO ()
putDiff old@(Outcome pid1 st1 _) new@(Outcome pid2 st2 _)
  | pid1 /= pid2 = putStrLn $ red $ "[Error] " <> pid1 <> " /= " <> pid2
  | otherwise = do
      putStrLn $ ppOutcome old
      putStrLn $ cmp st1 st2 $ ppOutcome' "  " new

cmp :: Status -> Status -> String -> String
cmp (Success _ _) (Success _ _) = yellow
cmp _             (Success _ _) = green
cmp (Success _ _) _             = red
cmp _             _             = yellow


run1 :: (?to :: Int) => Problem -> IO Outcome
run1 (Problem pid p) = do
  start <- getCurrentTime
  let exe = InferEt.runAny . flip InferEt.ect zero . fst $ gen p
  rM    <- timeout ?to exe
  let stM = case rM of
        Nothing                             -> Timeout
        Just(Just (InferEt.Success cexp log)) -> Success cexp log
        _                                   -> Failed
  end <- stM `seq` getCurrentTime
  let time = show $ diffUTCTime end start
  return $ Outcome pid stM time

timeout :: Int -> IO a -> IO (Maybe a)
timeout sec = Timeout.timeout (sec * 1000000)


{-# NOINLINE examples #-}
examples :: Benchmark
examples = unsafePerformIO $ Benchmark "example-distribution" <$> do
  fps <- sort . lines <$> readCreateProcess (shell "find examples -type f -name '*.imp' | sort") mempty
  forM fps $ \fp -> Problem fp <$> fromFile' fp


printBenchmarkTex :: (?to :: Int) => Benchmark -> IO ()
printBenchmarkTex (Benchmark _ ps) =
  forM_ ps $ \p -> do
    out <- run1 p
    putStrLn (texOutcome out)
  where
    texOutcome (Outcome n s t) = unwords
      [ escaped (takeBaseName n)
      ,"&"
      , math (texStatus s)
      ,"&"
      , math (take 5 t)
      , "\\\\"]
    texStatus Timeout       = "timeout"
    texStatus Failed        = "?"
    texStatus (Success c _) = texCExp c

    escaped []       = []
    escaped ('_':xs) = '\\' : '_' : escaped xs
    escaped (x:xs)   = x : escaped xs

    math s   = "$" <> s <> "$"
    texCExp = show . PP.pretty


printBenchmarkMd :: (?to :: Int) => Benchmark -> IO ()
printBenchmarkMd (Benchmark _ ps) = do
  createDirectoryIfMissing True "results/"
  putStrLn rule
  putStrLn heading
  putStrLn rule
  forM_ ps $ \p -> do

    out <- run1 p
    putStrLn (mdOutcome out)
    writeLog out
    putStrLn ""
  putStrLn rule

  where
    a <+> b = a <> " " <> b
    link txt target = "[" <> txt <> "](" <> target <> ")"
    code txt = "`" <> txt <> "`"

    (l1,l2,l3) = (40,160,5)

    rule = replicate l1 '-' <+> replicate l2 '-' <+> replicate l3 '-'

    heading = fill l1 "Example" <+> fill l2 "Result" <+> fill l3 "secs"

    mdOutcome (Outcome n s t) =
      fill l1 (link fn ("examples/" <> fn <> ".imp"))
      <+> fill l2 (link (code (mdStatus s)) ("results/" <> fn <> ".out"))
      <+> fill l3 (take 5 t)
      where
        fn = takeBaseName n

    mdStatus Timeout     = "timeout"
    mdStatus Failed      = "?"
    mdStatus (Success c _) = renderWidth l2 c

    writeLog (Outcome n s _) = writeFile ("results" </> takeBaseName n <> ".out") (logStr s) where
      logStr (Success _ l) = drawForest l
      logStr _             = ""

renderWidth :: PP.Pretty a => Int -> a -> String
renderWidth w c = PP.displayS (PP.renderPretty 1.0 w (PP.pretty c)) ""
