{-# LANGUAGE ImplicitParams #-}

import           System.Environment
import qualified System.Timeout             as Timeout
import           Data.Maybe                   (listToMaybe)

import           PWhile.InferEt
import           Data.PWhile.CostExpression
import           PWhile.DSL
import           PWhile.Parser

main :: IO ()
main = do
  as <- getArgs
  let ?to = 60
  case as of
    [fp] -> run1 fp Nothing
    (fp:i:_) -> run1 fp (Just (read i))
    _        -> print usage

  where
    usage = "<file>"

ec :: Program -> Maybe Int -> IO (Maybe (Result (CExp Rational)))
ec p i = listToMaybe <$> run (sel i) (ect (fst (gen p)) zero) where
  sel = maybe Any Pick


run1 :: (?to :: Int) => FilePath -> Maybe Int -> IO ()
run1 fp i = do
  programM <- fromFile fp
  case programM of
    Left err -> putStrLn (errorBundlePretty err)
    Right p  -> do
      rM  <- timeout ?to (ec p i)
      case rM of
        Nothing  -> putStrLn "[Timeout]"
        Just Nothing -> putStrLn "[Unknown]"
        Just (Just res) -> putStrLn (showResult res)

timeout :: Int -> IO a -> IO (Maybe a)
timeout sec = Timeout.timeout (sec * 1000000)
