{-# OPTIONS_GHC -fno-warn-incomplete-patterns #-}
{-# LANGUAGE FlexibleInstances, StandaloneDeriving, ViewPatterns, PatternSynonyms, OverloadedStrings, DeriveDataTypeable #-}
module Data.PWhile.Expression
  (
    -- expressions
    Var (..)
  , Expression
  , Exp
  , pattern Constant
  , pattern AddConst
  , variable
  , constant
  , variables
  , factor
  , posNeg
  , scale
  , cfactor
  , maxE
  , P.norm
  , Substitutable (..)
    -- distributions
  -- , Frac (..)
  , Dist (..)
  , discrete, dirac, rand, bernoulli, binomial, unif, hyper
  , prettyFrac
  )
where
import qualified Data.Set as S
import           Data.Data
import           Data.String
import           Data.List                    (partition, foldl1')

import qualified GUBS.Polynomial as P
import qualified Text.PrettyPrint.ANSI.Leijen as PP

newtype Var = Var String
  deriving (Eq, Ord, Read, Show, Data)

type Expression c = P.Polynomial Var c
type Exp = Expression Int

instance Num c => IsString (Expression c) where
  fromString = P.variable . Var

constant :: (Eq c, Num c) => c -> Expression c
constant = P.coefficient

variable :: Num c => Var -> Expression c
variable = P.variable

variables :: Expression c -> S.Set Var
variables = S.fromList . P.variables

factor :: Exp -> Exp -> (Exp, (Exp, Exp))
factor e1 e2 =
  case P.factorise [P.norm e1,P.norm e2] of
    Just ((c,m),[e1',e2']) -> (P.coefficient c * P.fromMono m, (e1',e2'))
    _                  -> (1,(e1,e2))

posNeg :: Exp -> (Exp, Exp)
posNeg e = (P.fromMonos p, P.fromMonos n) where
  (p,n) = partition ((>=0) . fst) (P.toMonos e)

scale :: Int -> Exp -> Exp
scale i e = P.fromMonos [(i*c,m) | (c,m) <- P.toMonos e]

cfactor :: Exp -> (Int, Exp)
cfactor e = case monos of
              [] -> (1,e)
              [(c,m)] -> (c, P.fromMonos [(1,m)])
              _       -> (f, P.fromMonos [(c `div` f, m) | (c,m) <- monos ])
  where
    monos = P.toMonos e
    f = foldl1' gcd [ c | (c,_) <- monos]

maxE :: (Eq c, Ord c, Num c) => [Expression c] -> Expression c
maxE [] = 0
maxE es = P.norm (foldl1 (P.zipCoefficientsWith (max 0) (max 0) max) es)

-- patterns
const_ :: (Ord v, Eq c, Num c) => P.Polynomial v c -> Maybe c
const_ (P.norm -> e)
  | null (P.variables e) = Just $ sum $ P.coefficients e
  | otherwise = Nothing

pattern Constant :: (Ord v, Eq c, Num c) => c -> P.Polynomial v c
pattern Constant i <- (const_ -> Just i)

addConst_ :: (Ord v, Eq c, Num c) => P.Polynomial v c -> (P.Polynomial v c,c)
addConst_ e = (e - P.coefficient c, c)
  where c = P.coefficientOf (P.fromPowers []) e

pattern AddConst :: (Ord v, Eq c, Num c) => P.Polynomial v c -> c -> P.Polynomial v c
pattern AddConst e i <- (addConst_ -> (e,i))



-- substitutions
----------------------------------------------------------------------

class Substitutable a where
  substitute :: Var -> Exp -> a -> a

instance (Eq c, Num c) => Substitutable (Expression c) where
  substitute v e = P.substitute s where
    s v' | v == v' = e'
         | otherwise = P.variable v'
    e' = fromIntegral `fmap` e

-- distributions
----------------------------------------------------------------------


deriving instance (Read v, Ord v) => Read (P.Monomial v)
deriving instance (Read c, Read v, Ord v) => Read (P.Polynomial v c)

deriving instance Data (P.Monomial Var)
deriving instance Data Exp

data Dist e
  = Discrete [(Exp,e)]
  | Rand e -- TODO => Uniform
  deriving (Data, Eq, Read, Ord, Show)

discrete :: [(Exp,e)] -> Dist e
discrete = Discrete

unif :: [e] -> Dist e
unif es = Discrete [(1,e) | e <- es]

rand :: Exp -> Dist Exp
rand = Rand

dirac :: e -> Dist e
dirac e = Discrete [(1,e)]

bernoulli :: (Exp, Exp) -> e -> e -> Dist e
bernoulli (a,b) e1 e2 = Discrete [(a, e1), (b, e2)]

binom :: Int -> Int -> Int
binom _ 0 = 1
binom 0 _ = 0
binom m k = binom (m-1) (k-1) * m `div` k


binomial :: (Int, Int) -> Int -> Dist Exp
binomial (pa,pb) n = Discrete [(fromIntegral $ binom n k * pa^k * (pb - pa)^k, fromIntegral k)
                              | k <- [0..n]]

hyper :: Int -> Int -> Int -> Dist Exp
hyper pN pK pn = Discrete [(fromIntegral $ binom pK k * binom (pN - pK) (pn - k), fromIntegral k)
                          | k <- [max 0 (pn + pK - pN) .. min pn pK]]

-- pretty
----------------------------------------------------------------------

instance PP.Pretty Var where
  pretty (Var v) = PP.text v

prettyFrac :: (Exp,Exp) -> PP.Doc
prettyFrac (n,d)
        | d == 1 = PP.pretty n
        | otherwise = PP.pretty n PP.<> PP.text "/" PP.<> PP.pretty d

instance PP.Pretty e => PP.Pretty (Dist e) where
  pretty (Discrete as) =
    PP.semiBraces [ prettyFrac (i,s) PP.<+> PP.text ":" PP.<+> PP.group (PP.pretty a)
                  | (i,a) <- as]
    where
      s = sum (fst `map` as)

  pretty (Rand e) = PP.text "rand" PP.<> PP.parens (PP.pretty e)
