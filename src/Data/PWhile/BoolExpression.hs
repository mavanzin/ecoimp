{-# OPTIONS_GHC -fno-warn-incomplete-patterns #-}
{-# LANGUAGE DeriveTraversable, PatternSynonyms, TypeSynonymInstances, StandaloneDeriving, ViewPatterns #-}
module Data.PWhile.BoolExpression
  (
    Conj (..)
  , lits, conjs, variables
  , Literal (..)
  , DNF (..)
  , BLit (..)
  , disj, conj
  , literals
  , toList
  , mapLiterals
  , traverseLiterals
  , top, bot
  , le, leq, ge, geq
  , lit
  , (.<),(.<=), (.>), (.>=), (.==)
  , BExp
  , pattern Bot, pattern Top
  , neg, (.&&), (.||), bigAnd, bigOr
  -- -- constraints
  -- , Constraint (..)
  -- , (.=>)
  )
where

import           Data.Data
import qualified Data.Set as S
import qualified Data.Set.Internal as SI
import qualified Text.PrettyPrint.ANSI.Leijen as PP

import           Data.PWhile.Expression hiding (variables)
import qualified Data.PWhile.Expression as E


-- DNFs

class Ord a => Literal a where
  literal :: a -> DNF a
  literal = lit
  lneg :: a -> a
  land :: a -> a -> Maybe (Either Bool a)
  land _ _ = Nothing
  lor  :: a -> a -> Maybe (Either Bool a)
  lor _ _ = Nothing

newtype Conj a = Conj { litsSet :: S.Set a } deriving (Eq, Ord, Read, Show, Data, Foldable)

newtype DNF a = Disj { conjSet :: S.Set (Conj a) } deriving (Eq, Ord, Read, Show, Data, Foldable)

pattern EmptySet :: S.Set a
pattern EmptySet = SI.Tip

pattern Top, Bot :: DNF a
pattern Top = Disj (SI.Bin 1 (Conj EmptySet) SI.Tip SI.Tip)
pattern Bot = Disj EmptySet

disj :: Ord l => [Conj l] -> DNF l
disj = Disj . S.fromList

conj :: Ord l => [l] -> DNF l
conj ls = disj [Conj (S.fromList ls)]

lit :: Ord l => l -> DNF l
lit l = disj [Conj (S.singleton l)]

conjs :: DNF l -> [Conj l]
conjs = S.toList . conjSet

lits :: Conj a -> [a]
lits = S.toList . litsSet

literals :: Ord l => DNF l -> S.Set l
literals = S.unions . S.map litsSet . conjSet

toList :: DNF l -> [[l]]
toList = map lits . conjs

mapLiterals :: Literal b => (a -> b) -> DNF a -> DNF b
mapLiterals f (Disj cs) = bigOr [ disj [Conj (f `S.map` litsSet c)] | c <- S.toList cs]

traverseLiterals :: (Applicative f, Literal b) => (a -> f b) -> DNF a -> f (DNF b)
traverseLiterals f (Disj cs) =
  bigOr <$> sequenceA [ disj <$> (: []) <$> traverseConj c | c <- S.toList cs]
  where
    traverseConj c = Conj <$> S.fromList <$> traverse f (S.toList (litsSet c))


top, bot :: DNF a
top = Top
bot = Bot

infixr 3 .&&
infixr 2 .||
(.&&),(.||) :: (Literal l) => DNF l -> DNF l -> DNF l
Top      .|| _ = Top
_        .|| Top = Top
Disj ds1 .|| Disj ds2 = Disj (ds1 `S.union` ds2)
_                 .&& Bot = Bot
Bot               .&& _ = Bot
Top               .&& dnf2 = dnf2
dnf1              .&& Top = dnf1
Disj cs           .&& dnf2 = walk (S.toList cs) where
  walk [Conj (S.toList -> ls1)]    = cnf [ ls1 `litsAnd` ls2
                                          | Conj (S.toList -> ls2) <- conjs dnf2]
    where
      cnf lls = disj $ [Conj $ S.fromList ls | ls <- lls, not (null ls)]
      [] `litsAnd` ks = ks
      (l:ls1) `litsAnd` ks =  litAnd l (ls1 `litsAnd` ks) []
      litAnd l [] ls = l:ls
      litAnd l (k:ks) ls = case l `land` k of
                             Nothing -> litAnd l ks (k:ls)
                             Just (Left True) -> ks ++ ls
                             Just (Left False) -> []
                             Just (Right l')  -> litAnd l' ks ls
  walk (c:cs') = disj [c] .&& dnf2 .|| disj cs' .&& dnf2

bigOr, bigAnd :: Literal l => [DNF l] -> DNF l
bigAnd = foldr (.&&) Top
bigOr = foldr (.||) Bot

neg :: Literal l => DNF l -> DNF l
neg Bot = Top
neg Top = Bot
neg (Disj cs) = bigAnd [ negConj c | c <- S.toList cs ] where
  negConj c = bigOr ((lit . lneg) `map` lits c)


-- Program Boolean Expressions
----------------------------------------------------------------------

data BLit = Exp :>=: Exp
  deriving (Eq, Ord, Read, Show, Data)

type BExp = DNF BLit

infix 4 .>=
infix 4 .>
infix 4 .<=
infix 4 .<
infix 4 .==

geq, ge, leq, le :: Exp -> Exp -> BLit
e1 `geq` e2 = p :>=: scale (-1) n
  where (p,n) = posNeg (e1 - e2)
-- e1 :>=: e2
e1 `ge` e2 = e1 `geq` (e2 + 1)
leq = flip geq
le = flip ge

(.>=), (.>), (.<=), (.<), (.==) :: Exp -> Exp -> BExp
Constant n1 .>= Constant n2 | n1 >= n2 = Top
                            | n1 < n2 = Bot
                            | otherwise = Bot
e1 .>= e2 | e1 == e2 = Top
          | otherwise = lit (e1 `geq` e2)
e1 .> e2 = e1 .>= (e2 + 1)
(.<=) = flip (.>=)
(.<) = flip (.>)
e1 .== e2 = lit (e1 `geq` e2) .&& lit (e2 `geq` e1)


instance Literal BLit where
  literal = lit
  lneg (e1 :>=: e2) = e2 `ge` e1
  (e1 :>=: e2) `land` (f1 :>=: f2)
    | E.Constant n <- (e1 - e2) + (f1 - f2) , n < 0 = Just (Left False)
    -- | E.AddConst e ke <- e1 - e2
    -- , E.AddConst f kf <- f1 - f2
    -- , e == f = Just (Right ((e1 + E.constant (min ke kf)) :>=: 0))
  _ `land` _ = Nothing

instance Substitutable BExp where
  substitute v s b =
    bigOr [ bigAnd [ substitute v s e1 .>= substitute v s e2
                   | e1 :>=: e2 <- S.toList cs ]
          | Conj cs <- conjs b]

variables :: BExp -> S.Set Var
variables g = S.unions [ E.variables a `S.union` E.variables b  | (a :>=: b) <- S.toList (literals g)]

-- pretty
----------------------------------------------------------------------

instance PP.Pretty BLit where
  pretty (e1 :>=: e2) =
    PP.group (PP.pretty e1) PP.</> PP.text "≥" PP.<+> PP.group (PP.pretty e2)

instance PP.Pretty l => PP.Pretty (DNF l) where
  pretty Top = PP.text "⊤"
  pretty Bot = PP.text "⊥"
  pretty dnf = lst " ∨ "  [ lst " ∧ " [ PP.group (PP.pretty l) | l <- lits c] | c <- conjs dnf]
    where lst s = PP.fillSep . PP.punctuate (PP.text s)
