{-# LANGUAGE ConstraintKinds #-}
module Data.PWhile.CostExpression where

import           Data.Ratio

import           Data.Functor.Identity (runIdentity)
import qualified Text.PrettyPrint.ANSI.Leijen as PP
import qualified Data.Set as S

import           Data.PWhile.BoolExpression hiding (variables)
import qualified Data.PWhile.BoolExpression as B
import qualified Data.PWhile.Expression as E


-- norms
----------------------------------------------------------------------

data Norm = Norm BExp E.Exp
  deriving (Eq, Ord, Read, Show)

norm :: BExp -> E.Exp -> Norm
norm _ e@(E.Constant _) = Norm Top e
norm g e                = Norm g e

oneN :: Norm
oneN = Norm Top 1

-- plusN :: Norm -> Norm -> Norm
-- plusN (Norm _ 0)   n@Norm{}     = n
-- plusN n@Norm{}     (Norm _ 0)   = n
-- plusN (Norm g1 e1) (Norm g2 e2) = norm (g1 .&& g2) (e1 + e2)

mulN :: Norm -> Norm -> Norm
mulN (Norm _ 1)   n@Norm{}     = n
mulN n@Norm{}     (Norm _ 1)   = n
mulN (Norm g1 e1) (Norm g2 e2) = norm (g1 .&& g2) (e1 * e2)

prodN :: [Norm] -> Norm
prodN = foldl mulN oneN

expN :: Norm -> Int -> Norm
expN (Norm g e) i = Norm g (product (replicate i e))

isZeroN :: Norm -> Bool
isZeroN (Norm _ e) = e == 0

isConstN :: Norm -> Bool
isConstN (Norm _ e) = null (E.variables e)

-- cost expressions
----------------------------------------------------------------------

class FromRatio c where fromRatio :: Rational -> c

type Coeff c = (Eq c, Num c, Ord c, FromRatio c)

instance FromRatio Rational where fromRatio = id


data CExp c =
  N c Norm
  | Div (CExp c) E.Exp
  | Plus (CExp c) (CExp c)
  | Sup (CExp c) (CExp c)
  | Cond BExp (CExp c) (CExp c)
  deriving (Eq, Ord, Read, Show)


-- fromExp :: (Eq c, Num c) => E.Exp -> CExp c
-- fromExp = E . fmap fromIntegral

nm :: (Eq c, Num c) => c -> Norm -> CExp c
nm 0 _                       = zero
nm _ (Norm Bot _)            = zero
nm c (Norm _ (E.Constant i)) = N (c * fromIntegral i) (norm Top 1)
nm c (Norm b e)              = N (c * fromIntegral k) (norm b e')
  where (k,e') = E.cfactor e

constant :: (Eq c, Num c) => c -> CExp c
constant c = nm c (Norm Top 1)


traverseCoeff :: (Coeff c', Applicative f) => (c -> f c') -> CExp c -> f (CExp c')
traverseCoeff f = walk where
  walk (N k n)      = nm <$> f k <*> pure n
  walk (Div c e)    = divBy <$> walk c <*> pure e
  walk (Plus c d)   = plus <$> walk c <*> walk d
  walk (Sup c d)    = sup <$> walk c <*> walk d
  walk (Cond b c d) = cond <$> pure b <*> walk c <*> walk d

mapCoeff :: Coeff c' => (c -> c') -> CExp c -> CExp c'
mapCoeff f = runIdentity . traverseCoeff (pure . f)

fromNorm :: (Eq c, Num c) => Norm -> CExp c
fromNorm = nm 1

zero :: Num c => CExp c
zero = N 0 (Norm Top 1)

one :: Num c => CExp c
one = N 1 (Norm Top 1)


plus :: Coeff c => CExp c -> CExp c -> CExp c
plus (Cond g c1 c2) (Cond g' c1' c2')
                             | g == g' = Cond g (c1 `plus` c1') (c2 `plus` c2')
plus (Div c1 e1) (Div c2 e2) | e1 == e2   = (c1 `plus` c2) `divBy` e1
plus (N k1 n1) (N k2 n2)     | n1 == n2   = nm (k1 + k2) n1
plus c1     c2               | c1 == zero = c2
                             | c2 == zero = c1
                             | otherwise  = Plus c1 c2

ramp :: (Eq c, Num c) => E.Exp -> CExp c
ramp e = nm 1 (norm (e .>= 0) e)

sum :: Coeff c => [CExp c] -> CExp c
sum = foldl plus zero

sup :: (Eq c, Num c) => CExp c -> CExp c -> CExp c
sup c1 c2 | c1 == zero = c2
          | c2 == zero = c1
          | c1 == c2 = c1
sup c1 c2 = Sup c1 c2

cond :: (Eq c, Num c) => BExp -> CExp c -> CExp c -> CExp c
cond Top c               _ = c
cond Bot _               d = d
cond g   (Cond g' c1 c2) d
  | c2 == d                = cond (g .&& g') c1 d
cond _   c               d
  | c == d                 = c
cond g   c               d = Cond g c d

simplify :: CExp Rational -> CExp Rational
simplify (N k n)      = nm k n
simplify (Div c e)    = simplify c `divBy` e
simplify (Plus c d)   = simplify c `plus` simplify d
simplify (Sup c d)    = simplify c `sup` simplify d
simplify (Cond b c d) =
  case cond b (simplify c) (simplify d) of
    Cond _ c' d' | c' == d' -> c'
    r                       -> r



guarded :: (Eq c, Num c) => BExp -> CExp c -> CExp c
guarded g c | c == zero = zero
            | otherwise = cond g c zero

-- oneV :: Num c => CExp c
-- oneV = N 1 (Norm Top "@@ONE@@")

predicate :: (Eq c, Num c) => BExp -> CExp c
predicate g = cond g one zero


-- class RatioFactor a where rfactor :: a -> (Rational, a)
-- instance RatioFactor Rational where rfactor r = (r,1)
-- instance RatioFactor Int where rfactor i = (fromIntegral i,1)

scale :: Coeff c => E.Exp -> CExp c -> CExp c
scale 0 _              = zero
scale 1 c              = c
scale (E.Constant k) c = scaleC (fromIntegral k) c
scale e (N k n)        = N k (norm (e .>= 0) e `mulN` n)
scale e (Div d f)      = scale e' d `divBy` f' where (_,(e',f')) = E.factor e f
scale e (Plus d1 d2)   = scale e d1 `plus` scale e d2
scale e (Sup d1 d2)    = scale e d1 `sup` scale e d2
scale e (Cond g d1 d2) = cond g (scale e d1) (scale e d2)

scaleC :: Coeff c => c -> CExp c -> CExp c
scaleC 0 _              = zero
scaleC 1 c              = c
scaleC k c              = mapCoeff (* k) c

divBy :: Coeff c => CExp c -> E.Exp -> CExp c
divBy c         _ | c == zero = zero
divBy c         1             = c
divBy (Div c e) f             = c `divBy` (e * f)
divBy c         f | f' == 1   = c'
                  | otherwise = Div c' f'
  where
    (k,f') = E.cfactor f
    c' = scaleC (fromRatio (1 % fromIntegral k)) c

variables :: CExp c -> S.Set E.Var
variables (N _ (Norm _ e)) = E.variables e
variables (Div c f)        = variables c `S.union` E.variables f
variables (Plus c d)       = variables c  `S.union` variables d
variables (Sup c d)        = variables c `S.union` variables d
variables (Cond g c d)     = B.variables g `S.union` variables c `S.union` variables d

instance E.Substitutable Norm where
  substitute s e (Norm g f) = norm (E.substitute s e g) (E.substitute s e f)

instance Coeff c => E.Substitutable (CExp c) where
  substitute s e (N k n)      = nm k (E.substitute s e n)
  substitute s e (Div c f)    = E.substitute s e c `divBy` E.substitute s e f
  substitute s e (Plus c d)   = E.substitute s e c `plus` E.substitute s e d
  substitute s e (Sup c d)    = E.substitute s e c `sup` E.substitute s e d
  substitute s e (Cond b c d) = cond (E.substitute s e b) (E.substitute s e c) (E.substitute s e d)

-- guarded norms

data GNorm c = GNorm BExp c Norm
  deriving Show

isConstGN :: (Eq c, Num c) => GNorm c -> Bool
isConstGN (GNorm g c n)
  | c == 0    = True
  | isZeroN n = True
  | g == Bot  = True
  | otherwise = isConstN n && S.null (B.variables g)

mulG :: (Eq c, Ord c, Num c) => GNorm c -> GNorm c -> GNorm c
mulG (GNorm g1 k1 n1) (GNorm g2 k2 n2) = GNorm (g1 .&& g2) (k1 * k2) (n1 `mulN` n2)

fromGNorm :: (Eq c, Ord c, Num c) => GNorm c -> CExp c
fromGNorm (GNorm Bot _ _)        = zero
fromGNorm (GNorm _ 0 _)          = zero
fromGNorm (GNorm _ _ (Norm _ 0)) = zero
fromGNorm (GNorm g k n)          = guarded g (N k n)

gNorms :: (Eq c, Num c) => CExp c -> [GNorm c]
gNorms = walk B.Top 1 where
  walk B.Bot _ _        = []
  walk b k (N k' e)     = [GNorm b (k * k') e]
  walk b k (Div c _)    = walk b k c
  walk b k (Plus c d)   = walk b k c ++ walk b k d
  walk b k (Sup c d)    = walk b k c ++ walk b k d
  walk b k (Cond g c d) = walk (b .&& g) k c ++ walk (b .&& neg g) k d

norms :: (Eq c, Num c) => CExp c -> S.Set Norm
norms c = S.fromList [n | GNorm _ _ n <- gNorms c]


-- pretty printers

instance PP.Pretty Norm where
  pretty (Norm _ e) = PP.text "<" PP.<> PP.hang 0 (PP.pretty e) PP.<> PP.text ">"

--   pretty (Norm g e) = PP.brackets (PP.hang 0 (PP.group (PP.pretty e) PP.</> PP.text "|" PP.<+> PP.group (PP.pretty g)))

instance (PP.Pretty c, Eq c, Num c) => PP.Pretty (CExp c) where
  pretty = pp id where
    pp _   (N 0 _)          = PP.text "0"
    pp _   (N 1 n)          = PP.pretty n
    pp _   (N k (Norm _ 1)) = PP.pretty k
    pp _   (N k n)          = PP.pretty k PP.<> PP.text "·" PP.<> PP.pretty n
    pp par (Div c e)        = par (infx (pp id c) "/" (PP.pretty e))
    pp par (Plus c d)       = par (infx (pp id c) "+" (pp id d))
    pp _   (Sup c d)        = ppFun "sup" [ PP.pretty c, PP.pretty d ]
    pp _   (Cond g c d)
      | d == zero           = PP.hang 1 (PP.brackets (PP.align (PP.pretty g))
                                          PP.</> PP.text "·" PP.<+> PP.group (pp PP.parens c))
      | otherwise           = PP.text "ite" PP.<> PP.tupled [ PP.pretty g, pp id c, pp id d ]
    ppFun f ls              = PP.text f PP.<> PP.tupled ls
    infx l s r              = PP.hang 1 (l PP.</> PP.text s PP.<+> r)


instance (PP.Pretty c, Eq c, Ord c, Num c) => PP.Pretty (GNorm c) where
  pretty = PP.pretty . fromGNorm
