module Data.PWhile.Program
  (
    C(..)
  , Label
  , Invariant
  , subPrograms
  , variables
  )
where

import           Data.Data
import qualified Data.Set as S
import qualified Text.PrettyPrint.ANSI.Leijen as PP

import           Data.PWhile.Expression hiding (variables)
import qualified Data.PWhile.Expression as E
import           Data.PWhile.BoolExpression hiding (variables)

type Label = Int
type Invariant = BExp

data C where
  -- base
  Abort  :: C
  Skip   :: C
  Tic    :: Exp -> C
  Ass    :: Var -> Dist Exp -> C
  -- comp
  NonDet :: C -> C -> C
  Choice :: [(Exp,C)] -> C
  Cond   :: Label -> Invariant -> BExp -> C -> C -> C
  While  :: Label -> Invariant -> BExp -> C -> C
  Seq    :: C -> C -> C
  deriving (Eq, Ord, Data)

instance Semigroup C where
  Skip <> c = c
  c <> Skip = c
  c1 <> c2  = Seq c1 c2

instance Monoid C where
  mempty           = Skip

instance PP.Pretty C where
  pretty Abort           = PP.text "Abort"
  pretty Skip            = PP.text "Skip"
  pretty (Tic e)         = PP.text "Tick" PP.<> PP.parens (PP.pretty e)
  pretty (Ass (Var v) d) = PP.text v PP.<+> PP.text ":~" PP.<+> PP.align (PP.pretty d)

  pretty (NonDet c1 c2) =
    PP.text "NonDet"
    PP.<$> PP.indent 2 (PP.braces (PP.pretty c1)
                        PP.<$> PP.braces (PP.pretty c2))
  pretty (Choice ls) =
    PP.align (PP.text "Choice"
               PP.<$> PP.indent 2 (PP.vcat [prettyFrac (w,s) PP.<> PP.text ":" PP.<+> PP.align (PP.pretty c)
                                           | (w,c) <- ls]))
    where s = sum [ w | (w,_) <- ls]
  pretty (Cond l _ g c1 c2) =
    PP.text (show l ++ ":")
    PP.<> PP.align (PP.text "If" PP.<> PP.parens (PP.pretty g) PP.<+> PP.text "Then"
                    PP.<$> PP.indent 2 (PP.pretty c1)
                    PP.<$> PP.text "Else"
                    PP.<$> PP.indent 2 (PP.pretty c2))
  pretty (While l _ g c1) =
    PP.text (show l ++ ":")
    PP.<> PP.align (PP.text "While" PP.<> PP.parens (PP.pretty g)
                    PP.<$> PP.indent 2 (PP.pretty c1))
  pretty (Seq c1 c2) = PP.pretty c1 PP.<$> PP.pretty c2

subPrograms :: C -> [C]
subPrograms c@(NonDet c1 c2)     = c : (subPrograms c1 ++ subPrograms c2)
subPrograms c@(Choice cs)        = c : concatMap (subPrograms . snd) cs
subPrograms c@(Cond _ _ _ c1 c2) = c : (subPrograms c1 ++ subPrograms c2)
subPrograms c@(While _ _ _ c1)   = c : subPrograms c1
subPrograms c@(Seq c1 c2)        = c : (subPrograms c1 ++ subPrograms c2)
subPrograms c                    = [c]

variables :: C -> S.Set Var
variables c = S.unions [ S.insert n (distVars d) | Ass n d <- subPrograms c]
  where
    distVars (Discrete cs) = S.unions [E.variables f `S.union` E.variables e | (f,e) <- cs]
    distVars (Rand e) = E.variables e
