{-# LANGUAGE RecordWildCards #-}
module PWhile.SampleEt
  ( Chart (..)
  , Measure (..)

  , measure
  , simulation
  , serialise
  , deserialise
  ) where


import           Data.Function              (fix)
import qualified Data.Map.Strict            as M
import           Data.Maybe                 (fromMaybe)
import           System.IO.Unsafe           (unsafePerformIO)
import           System.Random              (randomRIO)
import qualified Data.Sequence as Seq
import qualified Text.PrettyPrint.ANSI.Leijen as PP

import qualified GUBS.Polynomial            as Poly (evalWith)

import           Data.PWhile.BoolExpression
import qualified Data.PWhile.CostExpression as C
import           Data.PWhile.Expression     hiding (norm)
import           Data.PWhile.Program
import           PWhile.InferEt             (Result (..), ect, runAny)
import           PWhile.DSL                 (Program, gen)

import Debug.Trace

-- * Store

type Store = M.Map Var Int

update :: Var -> Exp -> Store -> Store
update v e st = M.insert v (evalExp e st) st

-- MS: For convenience we use default value '0'.
get :: Var -> Store -> Int
get v st = 0 `fromMaybe` M.lookup v st

evalExp :: Exp -> Store -> Int
evalExp e st = Poly.evalWith (`get` st) e

evalRExp :: Expression Rational -> Store -> Rational
evalRExp e st = Poly.evalWith (\ v -> fromIntegral (v `get` st)) e

evalBExp :: BExp -> Store -> Bool
evalBExp (Disj ds) st = any (\(Conj cs) -> all isGte cs) ds where
  isGte (a :>=: b) = evalExp a st >= evalExp b st

evalCExp :: C.CExp Rational -> Store -> Rational
evalCExp cexp st = ev cexp where
  ev (C.N k (C.Norm _ e)) = k * fromIntegral (evalExp e st)
  -- ev (C.Coeff k d)  = k * ev d
  ev (C.Div c e)    = ev c / fromIntegral (evalExp e st)
  ev (C.Plus c d)   = ev c + ev d
  ev (C.Sup c d)    = ev c `max` ev d
  ev (C.Cond g c d)
    | evalBExp g st = ev c
    | otherwise     = ev d

  -- evalMaxExp :: CExp Rational -> Rational
  -- evalMaxExp      (Max gs) = maximum [ evalGuardedSum g | g <- gs ]
  -- evalGuardedSum  (GS  m)  = M.foldlWithKey f 0 m
  -- f !acc g np
  --   | evalBExp g st = acc + evalNormPolyFrac np
  --   | otherwise     = acc
  -- evalNormPolyFrac (a :% b) = evalNormPoly a / evalNormPoly b
  -- evalNormPoly p            = Poly.evalWith (evalNorm) p
  -- evalNorm (Norm e)         = fromIntegral $ max 0 (evalExp e st)

-- * Sampling
-- XXX: MS: Uses unsafePerformIO. Since we sample below multiple times from a given input one has to be careful to
-- force the computation.

randomInt :: Int -> Int -> Int
randomInt a b = unsafePerformIO $ randomRIO (a,b)

sample :: [(Exp,a)] -> Store -> a
sample ds st = choose (randomInt 1 wsum) wdist where
  wsum  = sum [ w | (w,_) <- wdist ]
  wdist = [ (evalExp w st, a) | (w,a) <- ds ]

  choose :: Int -> [(Int,a)] -> a
  choose _ []         = error "Simulation.choose: sample size mismatch"
  choose n ((i,a):as)
    | i >= n          = a
    | otherwise       = choose (n-i) as

-- * Sampling Expected Cost

type Tick = Store -> Int

add,mul,sup :: Tick -> Tick -> Tick
add f g st = f st + g st
mul f g st = let a = f st in if a == 0 then 0 else g st
sup f g st = f st `max` g st

bracket :: BExp -> Tick
bracket g st = if evalBExp g st then 1 else 0

zero :: Tick
zero = const 0

norm :: Tick -> Tick
norm f = zero `sup` f

-- MS: Correct behaviour of rand?
-- rand(N) = uniform (0,n-1)
-- If N is negative the distribution is empty and treated below as skip.
rand2discrete :: Exp -> Store -> [(Exp,Exp)]
rand2discrete e st = [ (constant 1, constant z) | z <- [0..evalExp e st - 1] ]

-- MS: This is more or less 'ect' with expectation replaced by sampling.
eval :: C -> Tick -> Tick
eval cmd f = go cmd where
  go Abort       = zero
  go Skip        = f
  go (Tic e)     = norm (evalExp e) `add` f

  go (Ass v (Discrete [(1,e)])) = \st -> f $ update v e              st
  go (Ass v (Discrete ds))      = \st -> f $ update v (sample ds st) st
  go (Ass v (Rand e))           = \st -> eval (Ass v (Discrete $ rand2discrete e st)) f st
  go (Choice ds)                = \st -> eval (sample ds st) f st

  go (NonDet c d) = eval c f `sup` eval d f

  go (Cond _ i g c d) =
    (bracket (i .&& g)     `mul` eval c f) `add`
    (bracket (i .&& neg g) `mul` eval d f)
  go (While _ i g c) = fix $ \h ->
    (bracket (i .&& g)     `mul` eval c h) `add`
    (bracket (i .&& neg g) `mul` f)

  go (Seq c d) = eval c (eval d f)



-- * Measuared Expected Cost
--
-- XXX: MS: There is some imprecision.
--  * At the moment we round the value up to the next 'Int'. * The chosen quartils can be off, depending on the chosen
--  sample size, eg the chosen Median for 100 samples is the 51th one.

evalCExp' :: C.CExp Rational -> Store -> Int
evalCExp' c st = ceiling $ evalCExp c st

-- | Result of sampling the expected cost of 'sampleSize' times.
data Measure y = Measure
  { measureAvg    :: !y
  -- quartils
  , measureMax    :: !y
  , measureUpper  :: !y
  , measureMedian :: !y
  , measureLower  :: !y
  , measureMin    :: !y
  } deriving (Read, Show)

-- | Everything necessary to put on the chart.
data Chart y = Chart
  { chartTitle :: String
  --
  , chartSampleSize :: Int
  , chartInputStore :: [(String, Int)]
  , chartInputParam :: String
  -- inferred expected cost
  , chartCExp       :: C.CExp Rational
  , chartCExpPretty :: String
  , chartInferredEc :: [(Int, y)]
  -- measured expected cost
  , chartMeasuredEc :: [(Int, Measure y)]
  } deriving (Read, Show)

-- MS: force computation, because unsafePerformIO
{-# NOINLINE eval1 #-}
eval1 :: Program -> Store -> Int -> Int
eval1 p st dummy = dummy `seq` eval cmd zero st
  where (cmd,_) = gen p

measure :: Program -> Int -> Store -> Measure Int
measure program sampleSize store =
  let
    measureAvg    = sum runs `quot` sampleSize
    measureMax    = Seq.index runs (sampleSize - 1)
    measureUpper  = Seq.index runs (quartil * 3)
    measureMedian = Seq.index runs (quartil * 2)
    measureLower  = Seq.index runs quartil
    measureMin    = Seq.index runs 0
  in Measure{..}
  where
    runs    = Seq.unstableSort $ Seq.fromList $ eval1 program store <$> [1..sampleSize]
    quartil = sampleSize `quot` 4

inferCExp :: Program -> C.CExp Rational
inferCExp p = unsafePerformIO $ do
  r <- runAny $ ect cmd C.zero
  case r of
    Just (Success cexp _) -> return cexp
    _                     -> error "No bound inferred."
  where (cmd,_) = gen p

simulation :: Program -> String -> Int -> [(String,Int)] -> String -> Int -> Int -> [Int] -> Chart Int
simulation program title sampleSize inputStore inputParam minInput maxInput sampleInputs =
  let
    chartTitle      = title
    chartSampleSize = sampleSize
    chartInputStore = inputStore
    chartInputParam = inputParam
    chartCExp       = inferCExp program
    chartCExpPretty = show $ PP.pretty chartCExp
    chartInferredEc = [ (x, evalCExp' chartCExp (withStore x))        | x <- [minInput..maxInput] ]
    chartMeasuredEc = [ (x, withTrace x $  measure program sampleSize (withStore x)) | x <- sampleInputs ]
  in Chart{..}
  where
    withTrace x = trace $ "\rInput:\t" ++ show x
    withStore x = M.insert (Var inputParam) x $ M.fromList [ (Var v,z) | (v,z) <- inputStore ]

serialise :: Show a => FilePath -> Chart a -> IO ()
serialise fp = writeFile fp . show

deserialise :: Read a => FilePath -> IO (Chart a)
deserialise fp = read <$> readFile fp
