{-# LANGUAGE DeriveTraversable, GeneralizedNewtypeDeriving, MultiParamTypeClasses, PartialTypeSignatures,
             RecordWildCards, StandaloneDeriving #-}
{-# OPTIONS_GHC -Wall -Wno-partial-type-signatures #-}
module PWhile.Solve where


import           Control.Applicative          ((<|>))
import           Control.Monad.Except
import           Control.Monad.Trans          (MonadIO, lift, liftIO)
import qualified Data.Foldable                as F (toList)
import qualified Data.Map.Strict              as M
import           Data.Monoid                  ((<>))
import           Text.PrettyPrint.ANSI.Leijen (Pretty, pretty)
import qualified Text.PrettyPrint.ANSI.Leijen as PP

import           Control.Monad.Trace

import           GUBS.Algebra
import           GUBS.Interpretation          (Interpretation)
import qualified GUBS.Interpretation          as I
import           GUBS.Polynomial              (Polynomial)
import qualified GUBS.Polynomial              as P
import           GUBS.Solver                  as SMT
import           GUBS.Solver.Formula          as SMT
import           GUBS.Strategy                hiding (ProcT, Processor)
import qualified GUBS.Strategy                as S
import           GUBS.Utils

import           Data.PWhile.Expression       (Dist, Fun, Var)
import qualified Data.PWhile.Expression       as E


-- term
data Term f v c
  = Var v
  | Val c
  | Add (Term f v c) (Term f v c)
  | Mul (Term f v c) (Term f v c)
  | Neg (Term f v c)

  | Exp Dist
  | Nat (Term f v c)
  | Max (Term f v c) (Term f v c)

  | Fun f [Term f v c]
  deriving Show

instance Num c => Num (Term f v c) where
  (+)         = Add
  (*)         = Mul
  negate      = Neg
  fromInteger = Val . fromInteger
  abs         = error "GUBS.Rational.Num: abs undefined."
  signum      = error "GUBS.Rational.Num: signum undefined."


-- term folding

fromTermM :: Monad m => _ -> _ -> _ -> _ -> _ -> _ -> _ -> _ -> _ -> Term f v c -> m a
fromTermM var val add mul neg est nat sup fun = go where
  go (Var v)     = var v
  go (Val v)     = val v
  go (Add t1 t2) = go t1 >>= \t1' -> go t2 >>= \t2' -> add t1' t2'
  go (Mul t1 t2) = go t1 >>= \t1' -> go t2 >>= \t2' -> mul t1' t2'
  go (Neg t)     = neg =<< go t

  go (Exp d)     = est d
  go (Nat t)     = nat =<< go t
  go (Max t1 t2) = go t1 >>= \t1' -> go t2 >>= \t2' -> sup t1' t2'
  go (Fun f ts)  = fun f =<< mapM go ts

fromTerm :: _ -> _ -> _ -> _ -> _ -> _ -> _ -> _ -> _ -> Term f v c -> a
fromTerm var val add mul neg est nat sup fun = go where
  go (Var v)     = var v
  go (Val v)     = val v
  go (Add t1 t2) = add (go t1) (go t2)
  go (Mul t1 t2) = mul (go t1) (go t2)
  go (Neg t)     = neg (go t)

  go (Exp d)     = est d
  go (Nat t)     = nat (go t)
  go (Max t1 t2) = sup (go t1) (go t2)
  go (Fun f ts)  = fun f (map go ts)

funsDL :: Term a v c -> [(a, Int)] -> [(a, Int)]
funsDL = go where
  go Var{}       = id
  go Val{}       = id
  go (Add t1 t2) = go t1 . go t2
  go (Mul t1 t2) = go t1 . go t2
  go (Neg t1)    = go t1

  go Exp{}       = id
  go (Nat t1)    = go t1
  go (Max t1 t2) = go t1 . go t2
  go (Fun f ts)  = ((f,length ts) :)  . foldr ((.) . go) id ts


simplify :: (Eq c, Num c) => Term f v c -> Term f v c
simplify = fromTerm Var Val add' mul' Neg Exp Nat Max Fun where
  add' (Val c) (Val d)                      = Val (c + d)
  add' (Val c) d       | c == 0 = d
  add' c       (Val d) | d == 0 = c
  add' c       d                            = Add c d

  mul' (Val c) _       | c == 0 = zero
  mul' _       (Val d) | d == 0 = zero
  mul' (Val c) d       | c == 1 = d
  mul' c       (Val d) | d == 1 = c
  mul' c       d                            = Mul c d

simplify' :: (Ord v, Eq c, Num c) => Term f v c -> Term f v c
simplify' t = simplify $ maybe t toTerm' (toPoly' t) where
  toPoly' = 
    fromTermM
      (pure . P.variable)
      (pure . P.coefficient)
      (pure .: (+))
      (pure .: (*))
      (pure . negate)
      fail1
      fail1
      fail2
      fail2
  fail1 _   = Nothing
  fail2 _ _ = Nothing
  toTerm' = P.fromPolynomial Var Val

-- substitute / interpret

substitute :: (v -> Term f v' c) -> Term f v c -> Term f v' c
substitute s = fromTerm s Val Add Mul Neg Exp Nat Max Fun

interpret :: Ord f => _ -> _ -> _ -> _ -> _ -> _ -> _ -> Interpretation f (Term f I.Var c')
  -> Term f v c -> Term f v c'
interpret val add mul neg est nap sup inter = fromTerm Var val add mul neg est nap sup fun where
  fun f ts =
    case I.get inter f (length ts) of
      Just t  -> substitute (sigma (zip I.variables ts)) t
      Nothing -> Fun f ts
  sigma m k = M.fromList m M.! k

interpretFun :: Ord f => Interpretation f (Term f I.Var c) -> Term f v c -> Term f v c
interpretFun i = interpret Val Add Mul Neg Exp Nat Max i

interpretVal :: Ord f => (c -> Term f v c') -> Interpretation f (Term f I.Var c') -> Term f v c -> Term f v c'
interpretVal val i = interpret val Add Mul Neg Exp Nat Max i


-- utils

(.:) :: (t1 -> t2) -> (t3 -> t4 -> t1) -> t3 -> t4 -> t2
(.:) g f x y = g (f x y)

fmap2 :: (Functor f1, Functor f2) => (a -> b) -> f2 (f1 a) -> f2 (f1 b)
fmap2 = fmap . fmap

fmap3 :: (Functor f1, Functor f2, Functor f3) => (a -> b) -> f3 (f2 (f1 a)) -> f3 (f2 (f1 b))
fmap3 = fmap . fmap . fmap


-- constraints

data Constraint a = Constraint
  { premise     :: [a]
  , consequence ::  a }
  deriving (Eq, Ord, Show, Functor, Foldable, Traversable)


-- ert constraints

data Lit a = a :>=: a deriving (Show, Functor)

infix 4 .>:
(.>:) :: Num a => a -> a -> Lit a
a .>: b  = a :>=: (b + 1)

fromExp :: E.Exp -> ETerm
fromExp = go where
  go (E.V v)       = Var v
  go (E.E  d)      = Exp d
  go (E.Const i)   = Val (fromInteger i)
  go (E.Neg t)     = Neg (go t)
  go (E.Max t1 t2) = Max (go t1) (go t2)
  go (E.Add t1 t2) = Add (go t1) (go t2)
  go (E.Mul t1 t2) = Mul (go t1) (go t2)
  go (E.F f ts)    = Fun f (map go ts)

-- FIXME
expandDiscrete :: ETerm -> ETerm
expandDiscrete = fromTerm Var Val Add Mul Neg k Nat Max Fun where
  k (E.Discrete ps)                                = sumA [ Val (p % s) * fromExp' e | let s = sum [ i | (E.Const i, _) <- ps], (E.Const p,e) <- ps ]
  -- k (E.Bernoulli (E.Const n E.:% E.Const d) e1 e2) = Val (n%d) *  fromExp' e1 + (1- Val (n%d)) * fromExp' e2
  k t                                              = Exp t
  fromExp' = expandDiscrete . fromExp

type MTerm            = Term Fun I.Var Q
type ETerm            = Term Fun Var Q
type ConstraintSystem = [Constraint (Lit ETerm)]

fromErt :: [E.Constraint] -> ConstraintSystem
fromErt = map g where
  g (as E.:=>: c) = Constraint { premise = map h as, consequence = h c }
  h (l  E.:>=: r) = fromExp l :>=: fromExp r


-- constraint transformer

-- Assume t1,t2 are Fun free, then
-- P /\ t1 > t2 ==> C[t1] and P /\ t2 > t1 ==> C[t2] implies P ==> C[max(t1,t2)]
-- otherwise
--      t1 > t2 ==> C[t1] and      t2 > t1 ==> C[t2] implies P ==> C[max(t1,t2)]
unfoldMax :: Num c => Constraint (Lit (Term f v c)) -> [Constraint (Lit (Term f v c))]
unfoldMax Constraint { consequence = lhs :>=: rhs, ..} =
  [ Constraint (premise ++ as1 ++ as2) (c1 :>=: c2)
  | (as1, c1) <- go lhs
  , (as2, c2) <- go rhs ]
 where
  go t@Var{}     = pure' t
  go t@Val{}     = pure' t
  go (Add t1 t2) = lift2' Add (go t1) (go t2)
  go (Mul t1 t2) = lift2' Mul (go t1) (go t2)
  go (Neg t)     = lift1' Neg (go t)
  go t@Exp{}     = pure' t
  go (Nat t)     = lift1' Nat (go t)
  go (Max t1 t2) = do
    (as1, c1) <- go t1
    (as2, c2) <- go t2
    if   null (funsDL c1 $ funsDL c2 [])
      then [ ((c1 .>: c2) : as1 ++ as2, c1), ((c2 .>: c1) : as1 ++ as2, c2) ]
      else [ (as1 ++ as2, c1), (as1 ++ as2, c2) ]
  go (Fun f ts)  = liftN' f (map go ts)

  pure' t          = [ ([],t) ]
  lift1' f cs1     = [ (a1, f t1) | (a1,t1) <- cs1 ]
  lift2' f cs1 cs2 = [ (a1 ++ a2, f t1 t2) | (a1,t1) <- cs1, (a2,t2) <- cs2 ]
  liftN' f []  = pure' (Fun f [])
  liftN' f csN = fmap (Fun f) <$> k csN where
    k []       = [([],[])]
    k (c:cs) = do
      (ai,ti) <- c
      (as,ts) <- k cs
      return (ai ++ as, ti:ts)


toNonNegative :: Constraint (Lit (Term f v c)) -> Constraint (Term f v c)
toNonNegative = fmap (\(l :>=: r) -> Add l (Neg r))



-- FIXME: not correct in the general case
-- nat(f) == max(0,f)
-- t is a real-valued function, so t > 0 /= t -1 >= 0
-- t > 0  ==>  t >= 0 ==> nat(t) = t
-- 0 >= t ==  -t >= 0 ==> nat(t) = 0
eliminateNat :: Num c => Constraint (Term f v c) -> [Constraint (Term f v c)]
eliminateNat Constraint{..} = [ Constraint (premise ++ as) t | (as,t) <- go consequence ] where
  go t@Var{}     = pure' t
  go t@Val{}     = pure' t
  go (Add t1 t2) = lift2' Add (go t1) (go t2)
  go (Mul t1 t2) = lift2' Mul (go t1) (go t2)
  go (Neg t)     = lift1' Neg (go t)

  go t@Exp{}     = pure' t
  go (Nat t)     = [(as1 ++ [c1], c1) | (as1, c1) <- go t ] ++ [(as1 ++ [Neg c1], zero) | (as1, c1) <- go t ]
  go (Max t1 t2) = lift2' Max (go t1) (go t2)
  go (Fun f ts)  = liftN' f (map go ts)

  pure' t          = [ ([],t) ]
  lift1' f cs1     = [ (a1, f t1) | (a1,t1) <- cs1 ]
  lift2' f cs1 cs2 = [ (a1 ++ a2, f t1 t2) | (a1,t1) <- cs1, (a2,t2) <- cs2 ]
  liftN' f []  = pure' (Fun f [])
  liftN' f csN = fmap (Fun f) <$> k csN where
    k []       = [([],[])]
    k (c:cs) = do
      (ai,ti) <- c
      (as,ts) <- k cs
      return (ai ++ as, ti:ts)

-- ps ==> t >= C[t1]
-- ps ==> t >= C[t2]
-- ==>
-- ps ==> t >= C[max(t1,t2)]
-- eliminateNondet :: Num c => Constraint (Term f v c) -> [Constraint (Term f v c)]
-- eliminateNondet Constraint{..} = [ Constraint premise t | t <- go consequence ] where
--   go t@Var{}     = pure t
--   go t@Val{}     = pure t
--   go (Add t1 t2) = Add <$> (go t1) <*> (go t2)
--   go (Mul t1 t2) = Mul <$> (go t1) <*> (go t2)
--   go (Neg t)     = Neg <$> (go t)

--   go t@Exp{}     = pure t
--   go (Nat t)     = Nat <$> (go t)
--   go (Max t1 t2) = [ t1, t2 ]
--   go (Fun f ts)  = Fun f <$> (map go ts)


-- SMT

newtype SMT s a = SMT { runSMT_ :: ExceptT String (TraceT String (SolverM s)) a }

deriving instance SMTSolver s => Functor (SMT s)
deriving instance SMTSolver s => Applicative (SMT s)
deriving instance SMTSolver s => Monad (SMT s)
deriving instance SMTSolver s => MonadError String (SMT s)
deriving instance SMTSolver s => MonadTrace String (SMT s)

liftSMT :: SMTSolver s => SolverM s a -> SMT s a
liftSMT = SMT . lift . lift

runSMT :: MonadIO m => (SolverM s (Either String a1, _) -> IO (a2, _)) -> SMT s a1 -> S.ProcT f c m a2
runSMT solver = liftTrace . mapTraceT (liftIO . solver) . runExceptT . runSMT_

runZ3, runYices :: MonadIO m => SMT SMT.SMTLib2 a -> ProcT m (Either String a)
runZ3      = runSMT (SMT.runSMTLib2 "z3" ["-smt2", "-in"])
runYices   = runSMT (SMT.runSMTLib2 "yices-smt2" [])
runMinismt :: MonadIO m => SMT SMT.MiniSMT a -> ProcT m (Either String a)
runMinismt = runSMT SMT.miniSMT


-- polynomial encoding

type Term' f v s     = Term f v (SMTExpression s)
type Polynomial' v s = Polynomial v (SMTExpression s)

encode1 :: (Ord v, SMTSolver s) => Term' f v s -> SMT s (Polynomial' v s)
encode1 =
  fromTermM
    (pure . P.variable)
    (pure . P.coefficient)
    (pure .: (+))
    (pure .: (*))
    (pure . negate)
    fail1
    fail1
    fail2
    fail2
  where
    fail1 _   = throwError "fail1: Not a Polynom."
    fail2 _ _ = throwError "fail2: Not a Polynom."

decode1 :: SMTSolver s => Term' f v s -> SMT s (Term f v Q)
decode1 = liftSMT .
  fromTermM
    (pure . Var)
    (\t -> Val <$> P.evalWithM getValue t)
    (pure .: Add)
    (pure .: Mul)
    (pure .  Neg)

    (pure .  Exp)
    (pure .  Nat)
    (pure .: Max)
    (pure .: Fun)


freshReal' :: SMTSolver s => SolverM s (SMTExpression s)
freshReal' = do
  r <- P.variable <$> freshReal
  assert $ SMT.geqA r zero
  -- assert $ SMT.geqA (fromInteger 16) r
  return r

linear :: SMTSolver s => Int -> SMT s (Term' f I.Var s)
linear ar = liftSMT $ sumA <$> sequence (val : [ (*) <$> val <*> var v | v <- take ar I.variables ]) where
  val = (Val . P.variable) <$> freshReal
  var = pure . Var

mixed :: SMTSolver s => Int -> Int -> SMT s (Term' f I.Var s)
mixed deg ar = liftSMT $ polys where
  polys = sumA <$> sequence [ (*) <$> val <*> pure m | m <- monos ]
  monos = [ prod $ replicate p (Var v)  | ps <- pows, (v,p) <- ps ]
  pows  =
    map (filter (\(_,i) -> i>0)
    . zipWith (\v i -> (v,i)) (take ar I.variables))
    . filter (\ps -> sum ps <= deg)
    . sequence $ replicate ar [0..deg]
  val = (Val . P.variable) <$> freshReal


difference :: SMTSolver s => Int -> SMT s(Term' f I.Var s)
difference ar = liftSMT $ do
  -- k <- (Val . P.variable) <$> freshInt
  fmap sumA $ sequence $
    c0 :
     [ (*) <$> cn <*> nat (Var x - Var y)  | x <- vars, y <- vars, x /= y ] ++
     [ (*) <$> cn <*> nat (Var x)          | x <- vars ]
     -- [ (*) <$> val <*> nat (Var x + k)          | x <- vars ]
     -- [ (*) <$> val <*> nat (Var x + one)          | x <- vars ]
     -- [ (*) <$> val <*> nat (Var x + k)          | x <- vars ]
     -- [ (*) <$> val <*> nat (negate $ Var x) | x <- vars ]
  where
  nat  = pure . Nat
  vars = [ v | v <- take ar I.variables]
  cn  = Val <$> do { r <- freshReal'; assert (SMT.geqA 1 r); return r }
  c0  = Val <$> freshReal' 


-- Solving Inequality Constraints: p_i >= 0 ==> q >=0
--
-- To show that q >= 0 follows from p_i >= 0 we try to construct a witness st. M(p_i) == q_i;
-- where M(p_i) are operations preserving non-negativity and (==) is equality of coefficients


weight :: (SMTSolver s, Ord v) => Polynomial' v s -> SolverM s (Polynomial' v s)
weight p = (*p) <$> do
  r <- P.variable <$> freshReal
  assert $ SMT.geqA r zero
  -- assert $ SMT.geqA (fromInteger 4) r
  return (P.coefficient r)


farkas :: (Ord v, SMTSolver s) => [Polynomial' v s] -> SolverM s (Polynomial' v s)
farkas ps = sumA <$> mapM weight (one : ps)

handelman :: (Ord v, SMTSolver s) => Int -> [Polynomial' v s] -> SolverM s (Polynomial' v s)
handelman deg ps = sumA <$> mapM weight [ prod ms | ms <- replicateM deg (one:ps) ]


entscheide1
  :: (Ord v, SMTSolver s)
  => _
  -> Constraint (Polynomial' v s)
  -> SMT s ()
entscheide1 str Constraint{..} = liftSMT $ do
  premise' <- str premise
  SMT.assert $ SMT.smtAll (uncurry SMT.eqA) $
    P.zipCoefficients premise' consequence

entscheide
  :: (Ord v, SMTSolver s)
  => _
  -> [Constraint (Polynomial' v s)]
  -> SMT s ()
entscheide str = mapM_ (entscheide1 str)


-- processor

type Processor m = S.Processor Fun MTerm ConstraintSystem m
type ProcT m a   = S.ProcT Fun MTerm m a

liftNonnegative :: Monad m => S.Processor Fun MTerm [Constraint ETerm] m -> Processor m
liftNonnegative p cs = do
  r <- p (fmap toNonNegative cs)
  case r of
    NoProgress  -> return NoProgress
    Progress [] -> return (Progress [])
    Progress _  -> return NoProgress


dio' :: MonadIO m => _ -> _ -> S.Processor Fun MTerm [Constraint ETerm] m
dio' _    _    [] = return NoProgress
dio' tmpl strt cs = do
  tint <- getInterpretation
  let
    cs1 = fmap2 (interpretFun tint) cs
    sig = M.fromList . fmap (\(k,a) -> ((k,a),a)) . ($ []) . foldr ((.) . funsDL) id $ concatMap F.toList cs1
    solver = do
      aint <- I.Inter <$> traverse tmpl sig
      let
        cs2 = fmap2 (interpretVal (Val . P.coefficient) aint) cs1
        eli = eliminateNat
        cs3 = concatMap eli cs2
      cs4 <- mapM (traverse encode1) cs3

      -- logBlk "Polynomial Constraints:" $ forM_ cs4 $ logMsg . PP.pretty

      entscheide strt cs4
      ifM (liftSMT checkSat)
        (traverse decode1 aint)
        (throwError "No Assingment found.")
  -- mi <- liftIO $ runZ3 solver
  -- mi <- liftIO $ runYices solver
  -- mi <- runZ3 solver
  mi1 <- runZ3 solver
  mi2 <- runYices solver
  case mi1 <|> mi2 of
    Left msg -> logMsg msg                       >> return NoProgress
    Right  t -> modifyInterpretation (I.union t) >> return (Progress [])


dio :: MonadIO m => _ -> _ -> Processor m
dio tmpl strt = liftNonnegative (dio' tmpl strt)

-- FIXME: GUBS.Algebra.read
-- fails for negative integers (- 1) and z3 real-rational output (/ 25.0 24.0)

unfoldDiscrete :: Monad m => Processor m
unfoldDiscrete = return . Progress . fmap3 expandDiscrete

unfoldNondet :: Monad m => Processor m
unfoldNondet = return . Progress . concatMap unfoldMax


proc :: Processor IO
proc =
  withLog "UNFOLD" unfold ==> withLog "INFER" infer
  where

  unfold =
    unfoldDiscrete ==> unfoldNondet
  infer =
    tmpl "LI-F"       (dio linear farkas)
    ==> tmpl "MI2-H"  (dio (mixed 2) (handelman 3))
    ==> tmpl "DIFF-F" (dio difference farkas)

  tmpl :: MonadIO m => String -> Processor m -> Processor m
  tmpl s p = logAs s (timed $ try p)

  logAs   s p cs   = logBlk (s++"...") (p cs)
  withLog s p cs   = logBlk (s++"...") $ logConstraints cs *> p cs <* logConstraints cs <* logInterpretation


solve :: [Constraint (Lit ETerm)] -> IO (Answer Fun MTerm [Constraint (Lit ETerm)], ExecutionLog)
solve = flip S.solveWithLog proc


 -- log

logConstraints :: Monad m => ConstraintSystem -> ProcT m ()
logConstraints cs = logBlk "Constraints" $ do
  i <- getInterpretation
  forM_ cs (logConstraint i)

logConstraint :: Monad m => Interpretation Fun MTerm -> Constraint (Lit ETerm) -> ProcT m ()
logConstraint i Constraint{..} =
  logMsg $
    ppAnd premise PP.<+> ppImplies PP.<$> PP.indent 2 (ppConsequence consequence)
  where
  ppConsequence (l :>=: r) =
    PP.pretty l PP.<+> PP.hang 2 (
      PP.text "=" PP.<+> pp l
      PP.</> PP.text ">=" PP.<+> pp r
      PP.</> PP.text "="  PP.<+> PP.pretty r )
    where pp = pretty . interpretFun i

logInterpretation :: Monad m => ProcT m ()
logInterpretation = logBlk "Interpretation" $ do
  fs <- I.toList <$> getInterpretation
  forM_ fs $ \((f,i),t) -> logMsg $
    PP.pretty (Fun f [Var v | v <- take i I.variables] :: MTerm) <> PP.text " = " <> PP.pretty t


-- pretty

ppAnd :: PP.Pretty a => [a] -> PP.Doc
ppAnd = PP.hcat . PP.punctuate (PP.text " ∧ ") . map PP.pretty

ppImplies, ppGeq :: PP.Doc
ppImplies = PP.text "⇒"
ppGeq     = PP.text "≥"

instance Pretty a => Pretty (Lit a) where
  pretty (l :>=: r) = pretty l PP.<+> ppGeq PP.<+> pretty r

instance (PP.Pretty f, PP.Pretty v, PP.Pretty c, Ord v, Num c, Eq c) => PP.Pretty (Term f v c) where
  pretty = pp id . simplify' where
    pp _   (Var v)     = PP.pretty v
    pp _   (Val v)     = PP.pretty v
    pp par (Add e1 e2) = ppInfix par "+" e1 e2
    pp par (Mul e1 e2) = ppInfix par "*" e1 e2
    pp par (Neg e)     = ppPrefix par "-" e

    pp _   (Exp d)     = PP.text "E" PP.<> PP.parens (PP.pretty d)
    pp _   (Nat e1)    = ppFun (PP.text "nat") [e1]
    pp _   (Max e1 e2) = ppFun (PP.text "max") [e1,e2]
    pp _   (Fun f es)  = ppFun (PP.pretty f) es

    ppPrefix _ n e      = PP.text n PP.<> pp PP.parens e
    ppInfix par n e1 e2 = nest (pp par e1 PP.</> PP.text n PP.<+> pp par e2)
    ppFun n es = PP.group (n PP.<> PP.group (PP.tupled [PP.pretty e | e <- es]))
    nest = PP.nest 2

instance PP.Pretty a => PP.Pretty (Constraint a) where
  pretty Constraint{..} = PP.group (pp premise consequence) where
    pp [] c = PP.pretty c
    pp as c = PP.nest 2 (ppAnd as PP.<+> ppImplies PP.<$> PP.pretty c )


