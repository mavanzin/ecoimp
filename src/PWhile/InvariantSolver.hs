module PWhile.InvariantSolver
  (
    UpperInvariant (..)
  , InvariantSolver (..)
  )
where

import           Text.PrettyPrint.ANSI.Leijen (Pretty, pretty, (<+>), (</>))
import qualified Text.PrettyPrint.ANSI.Leijen as PP
import           Data.Ratio

import           PWhile.Util
import           Data.PWhile.BoolExpression
import           Data.PWhile.CostExpression

-- newtype Norm = Norm Exp
--   deriving Show

data UpperInvariant =
  UpperInvariant
  { inv  :: BExp
  , cnd  :: BExp
  , cost :: CExp Rational
  , step :: [CExp Rational]
  , cont :: CExp Rational
  , limt :: [Norm]}
  deriving Show

class InvariantSolver s where
  entails :: s -> DNF BLit -> DNF BLit -> Bool
  solve :: s -> UpperInvariant -> Maybe (CExp Rational)


-- pretty printers


instance {-# OVERLAPPING #-} PP.Pretty Rational where
  pretty r
    | d == 1 = PP.pretty n
    | d == 0 = PP.pretty d
    | otherwise = PP.pretty n <> PP.text "/" <> PP.pretty d
    where (n,d) = (numerator r, denominator r)

instance Pretty UpperInvariant where
  pretty ui  = ppStep PP.<$$> ppCont where
    -- toPoly ns = P.fromMono (P.fromPowers ns) :: P.Polynomial Norm Int
    ppStep   = ppPrem (inv ui .&& cnd ui) <+> pretty (cost ui)
               </> ppAdd <+> ppHi (step ui)
               </> ppLeq <+> ppHi (limt ui)
    ppCont   = ppPrem (inv ui .&& neg (cnd ui)) <+> pretty (cont ui)
               </> ppLeq <+> ppHi (limt ui)
    ppPrem b = pretty b <+> ppImp
    ppHi gi  = PP.char 'h' <> PP.tupled (fmap (PP.group . pretty) gi)
