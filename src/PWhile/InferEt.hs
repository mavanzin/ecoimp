{-# OPTIONS_GHC -Wno-unused-local-binds -Wno-orphans -Wno-unused-top-binds -Wno-type-defaults #-}
{-# LANGUAGE DeriveFunctor, FlexibleContexts, PatternSynonyms, ViewPatterns, TupleSections
           , NoOverloadedStrings #-}
module PWhile.InferEt
  (
    Result (..)
  , FilterResult (..)
  , Log
  , SolveM
  , run, runAny
  , ect, evt
  , showResult
  ) where

import           Data.List                    (nub,delete)
import           Control.Applicative
import           Control.Monad.Except
import qualified Control.Monad.State          as St
import           Data.Maybe                   (maybeToList, catMaybes, listToMaybe)
import qualified Data.Map.Strict              as M
import           Data.Tree                    (Forest)
import qualified Data.Set                     as S
import qualified GUBS.Polynomial as P

import qualified ListT                        as L
import qualified Text.PrettyPrint.ANSI.Leijen as PP

import           Control.Monad.Trace
-- import           Debug.Trace                  as D

import           PWhile.InvariantSolver       hiding (entails)
import           PWhile.InvariantSolver.Naive hiding (entails)
import qualified PWhile.InvariantSolver       as Solver
import           Data.PWhile.BoolExpression
import qualified Data.PWhile.BoolExpression   as B
import qualified Data.PWhile.Expression       as E
import qualified Data.PWhile.CostExpression   as C
import           Data.PWhile.Program
import qualified PWhile.Util                  as U
import Data.Either (isRight)


--- * Solver Monad ---------------------------------------------------------------------------------------------------

type CExp = C.CExp Rational

type MemoT = St.StateT (M.Map (Knd,C,CExp) CExp)


runMemoT :: Monad m => MemoT m v -> m v
runMemoT = flip St.evalStateT M.empty

memoized :: (Knd -> C -> CExp -> SolveM CExp) -> Knd -> C -> CExp -> SolveM CExp
memoized f k c e = do
  memo <- liftMemoT St.get
  case M.lookup (k,c,e) memo of
    Nothing -> do
      v <- f k c e
      liftMemoT $ St.modify (M.insert (k,c,e) v)
      return v
    Just v  -> return v

newtype SolveM a = SolveM { runSolveM_ :: ExceptT String (TraceT String (L.ListT (MemoT IO))) a }
  deriving (Applicative, Functor, Monad, MonadError String, MonadTrace String, MonadIO)

-- prvoides a 'Stream' of possible failing computations with logging
runSolveM' :: SolveM a -> L.ListT (MemoT IO) (Either String a, Forest String)
runSolveM' = runTraceT . runExceptT . runSolveM_

runSolveM :: SolveM a -> IO [(Either String a, Forest String)]
runSolveM = runMemoT . L.toList . runSolveM'



-- * Alternative

instance Alternative SolveM where
  empty     = SolveM $ ExceptT $ TraceT empty
  ma <|> mb = do
    let
      a = runExceptT (runSolveM_ ma)
      b = runExceptT (runSolveM_ mb)
    SolveM $ ExceptT $ a <|> b

liftMemoT :: MemoT IO a -> SolveM a
liftMemoT = SolveM . lift . lift . lift

liftListT :: L.ListT (MemoT IO) a -> SolveM a
liftListT = SolveM . lift . lift

alternative :: Foldable f => f a -> SolveM a
alternative = liftListT . L.fromFoldable

first :: Monad m => (a -> Bool) -> L.ListT m a -> m [a]
first p = go where
  go m = do
    aM <- L.uncons m
    case aM of
      Nothing    -> return []
      Just (a,l) -> if p a then return [a] else go l

choose :: SolveM a -> SolveM a
choose ma = do
  res <- liftMemoT (go (runSolveM' ma) [])
  case res of
    Left ls -> U.logBlk "unsucessful trial(s)" $ mapM putTrace ls >> throwError "All branches failed"
    Right (r, l) -> putTrace l >> return r
  where
    go s ls = do
      a <- L.uncons s
      case a of
        Nothing -> return (Left ls)
        Just ((Left _,l),s') -> go s' (l:ls)
        Just ((Right r,l),_) -> return (Right (r,l))

-- ** Logging

logResult :: PP.Pretty a => String -> SolveM a -> SolveM a
logResult n ma =  ma >>= \a -> U.logMsg2 n a >> return a

logData :: PP.Pretty a => String -> a -> SolveM ()
logData = U.logMsg2

logBlk :: PP.Pretty a => String -> SolveM a -> SolveM a
logBlk s m = U.logBlk s (m >>= \a -> U.logMsg a >> return a)

debugM :: (Applicative f, PP.Pretty e) => String -> e -> f e
debugM = U.debugMsgA "InferEt"

debug :: (PP.Pretty e) => String -> e -> e
debug = U.debugMsg "InferEt"


-- ** Result
type Log = Forest String

data Result a = Failure String Log
              | Success a Log

instance PP.Pretty a => PP.Pretty (Result a) where
  pretty (Failure m l) = U.renderLog l PP.<$$> PP.red (PP.text "[Failure]" PP.<+> PP.text m)
  pretty (Success f l) = U.renderLog l PP.<$$> PP.green (PP.text "[Success]" PP.<+> PP.pretty f)


showResult :: PP.Pretty a => Result a -> String
showResult = show . PP.pretty

-- instance PP.Pretty [Result] where
--   pretty rs = PP.vcat
--     [ PP.pretty "***" PP.<//> PP.pretty r PP.<//> PP.pretty "<<<" PP.<//> PP.text "" | r <- rs]

data FilterResult
  = Any       -- return first successful result
  | Take Int  -- take first n results
  | Pick Int  -- pick ith result
  | All       -- take all results
  deriving (Show, Eq)

run :: FilterResult -> SolveM a -> IO [Result a]
run t = fmap (map toResult) . runMemoT . select t . runSolveM' where
  select Any = first success where
    success (fst -> Right{}) = True
    success _                = False
  select (Take i) = L.toList . L.take i
  select (Pick i) = L.toList . L.drop (pred i) . L.take i

  select All      = L.toList
  toResult (Left err, l) = Failure err l
  toResult (Right a, l) = Success a l

runAny :: SolveM a -> IO (Maybe (Result a))
runAny m = listToMaybe <$> run Any m

-- cost functions
----------------------------------------------------------------------

varGNorms :: (Eq c, Num c) => C.CExp c -> [C.GNorm c]
varGNorms = filter (not . C.isConstGN) . C.gNorms

-- templates
----------------------------------------------------------------------

type NormTemplate = P.Polynomial C.Norm Int

templateToNorms :: NormTemplate -> [C.Norm]
templateToNorms p = nub $
  filter (not . zeroN) [ C.prodN [ C.expN n k | (n,k) <- P.toPowers m ]
                       | (_,m) <- P.toMonos p]
  where zeroN (C.Norm _ e) = e == 0

squash :: [C.Norm] -> [C.Norm]
squash = foldl ins [] where
  ins [] n     = [n]
  ins (C.Norm gm (E.AddConst m' cm):ns) (C.Norm _ (E.AddConst n' cn))
    | n' == m' = ins ns (C.Norm gm (n' + E.constant (max cn cm)))
  ins (m:ns) n = m : ins ns n

normTemplateEq :: NormTemplate -> NormTemplate -> Bool
p1 `normTemplateEq` p2 = and $ P.coefficients $
  P.zipCoefficientsWith (const False) (const False) (\ _ _ -> True) p1 p2

norm :: C.Norm -> NormTemplate
norm (C.Norm _ (E.Constant _)) = P.coefficient 1
norm n                         = P.variable n

normFromExp :: E.Exp -> NormTemplate
normFromExp e = norm (C.Norm (e .>= 0) e)

normFromBExp :: BExp -> NormTemplate
normFromBExp b = sum [ normFromExp (e1 - e2 + 1)
                     | e1 :>=: e2 <- S.toList (literals b) ]

substituteM :: (C.Norm -> SolveM NormTemplate) -> NormTemplate -> SolveM NormTemplate
substituteM s = P.fromPolynomialM s (pure . P.coefficient)

template :: String -> SolveM NormTemplate -> SolveM NormTemplate
template name = logBlk ("[template ("++name++")]")


-- estimating expectations
----------------------------------------------------------------------

finiteExpectation :: [(E.Exp, e)] -> (e -> SolveM CExp) -> SolveM CExp
finiteExpectation [(_,e)] f = f e
finiteExpectation ls f =
  C.divBy <$> (C.sum <$> sequence [C.scale w <$> f e | (w,e) <- ls'])
          <*> pure (sum [e | (e,_) <- ls'])
  where ls' = [ (fmap fromIntegral w, e) | (w,e) <- ls ]

boundedSum :: (E.Var,CExp) -> BExp -> (E.Exp, E.Exp) -> SolveM CExp
boundedSum (i,f) ctx (l,o) = bs f
  where
    bs g                                    = logBlk "bounded-sum" $ do
      logData "expression" (PP.text "sum"
                            PP.<> PP.braces (PP.pretty g
                                              PP.<+> PP.text "|"
                                              PP.<+> PP.pretty l
                                              PP.<+> PP.text "<="
                                              PP.<+> PP.pretty i
                                              PP.<+> PP.text "<="
                                              PP.<+> PP.pretty o))
      logData "context" ctx
      t <- template "guarded" (pure (sum [ nn * ng
                                         | C.GNorm b _ n <- varGNorms (C.guarded ctx g)
                                         , let nn = norm n
                                         , let ng = sum [ normFromExp (e1 - e2 + 1)
                                                        |  e1 :>=: e2 <- S.toList (literals b) ]]))
           -- <|> template "iterate" (pure (sum [ norm (E.substitute i 1 n) * normFromExp (E.variable i) * normFromExp (E.variable i)
           --                                   | C.GNorm _ _ n <- varGNorms g]))
      let ns = templateToNorms t
          vi = E.variable i
          uiDown = UpperInvariant {
              inv  = ctx .&& o .>= vi
            , cnd  = vi .>= l
            , cost = g
            , step = (E.substitute i (vi - 1) . C.fromNorm)  `map` ns
            , cont = C.zero
            , limt = ns }
          uiUp = UpperInvariant {
              inv  = ctx .&& vi .>= l
            , cnd  = o .>= vi
            , cost = g
            , step = (E.substitute i (vi + 1) . C.fromNorm)  `map` ns
            , cont = C.zero
            , limt = ns }
      (ui,dir,s) <- alternative [(uiDown, "down",o), (uiUp, "up",l)]
      logData ("Invariant("++dir++")") ui
      E.substitute i s <$> (debugM "result" =<< solveInvariant ui)


entails :: DNF BLit -> DNF BLit -> Bool
entails ctx c = debug (U.renderPretty (PP.pretty ctx <> U.ppEntails <> (PP.pretty c))) (Solver.entails (NaiveSolver 1 1) ctx c)

uniformExpectation :: (E.Exp,E.Exp) -> (E.Exp -> CExp) -> SolveM CExp
uniformExpectation (lo,hi) (( $ (E.variable (E.Var "@i"))) -> f) = do
  let ctx = hi .>= lo
  C.guarded ctx <$> ue ctx (lo,hi) f
    where
      v = E.Var "@i"

      scaleFrac (n,d) = C.scale n . flip C.divBy d

      ue ctx rng g =
        logBlk "uniformExpectation" $ do
        logData "interval" rng
        logData "pf" ( C.divBy (C.one :: C.CExp Rational) (hi - lo + 1) )
        logData "context" ctx
        logData "f" g
        walk ctx rng g
      walk _ (l,h) g
        | not (v `S.member` C.variables g) = pure (scaleFrac (h - l + 1, hi-lo+1) g)
      -- walk ctx (l,h)  g
      --   | not (v `S.member` C.variables g
      --         || v `S.member` B.variables ctx) = pure (scaleFrac (h - l + 1, hi-lo+1) g)
      walk ctx rng (C.Plus g1 g2)
        | not (v `S.member` B.variables ctx) = C.plus <$> ue ctx rng g1 <*> ue ctx rng g2
      walk ctx rng (C.Cond b g1 g2)
        | g2 == C.zero = ue ( b .&& ctx)  rng g1
        | g1 == C.zero = ue ( neg b .&& ctx)  rng g2
        | ctx `entails` b = ue ( b .&& ctx)  rng g1
        | ctx `entails` neg b = ue ( neg b .&& ctx)  rng g2
        | otherwise = C.plus <$> ue (b .&& ctx) rng g1 <*> ue (neg b .&& ctx) rng g2

      -- TODO: sensible extend to full DNFS?
      walk (toList -> [cs]) (l,h) g
        | Just (l',ctx') <- refinedLB cs [] = ue ctx' (l',h) g
        where
          refinedLB [] _ = Nothing
          refinedLB ( (e1 :>=: e2) : cs1) cs2
            | l' <- (-1) * (e1 - e2 - (E.variable v)) , not (v `S.member` E.variables l')
            , ctx' <- bigAnd (lit `map` (cs1 ++ cs2))
            , entails ctx' (l' .>= l)
            , not (entails ctx' (l .>= l')) = Just (l',ctx')

          refinedLB (c : cs1) cs2 = refinedLB cs1 (c:cs2)

      walk (toList -> [cs]) (l,h) g
        | Just (h',ctx') <- refinedUB cs [] = ue ctx' (l,h') g
        where
          refinedUB [] _ = Nothing
          refinedUB ( (e1 :>=: e2) : cs1) cs2
            | h' <- e1 - e2 + (E.variable v) , not (v `S.member` E.variables h')
            , ctx' <- bigAnd (lit `map` (cs1 ++ cs2))
            , entails ctx' (h .>= h')
            , not (entails ctx' (h' .>= h)) = Just (h',ctx')

          refinedUB (c : cs1) cs2 = refinedUB cs1 (c:cs2)

      walk ctx (l,h) (C.N k (C.Norm g e))
        | linearCase = pure $
          scaleFrac (h - l + 1,hi-lo+1) $ C.N k (C.Norm g e') `C.plus` C.N (k/2) (C.Norm g (E.substitute v (h - l) ev))
        where
          linearCase = all (all lin . P.toPowers . snd) (P.toMonos e)
            where lin (vi,m) = vi /= v || m <= 1
          e' = E.substitute v 0 e
          ev = e - e'

      walk ctx (l,h) g = C.divBy <$> boundedSum (v, g) ctx (l,h) <*> pure (hi - lo + 1)


expectation :: E.Dist E.Exp -> (E.Exp -> CExp) -> SolveM CExp
expectation (E.Discrete ls) f = finiteExpectation ls (return . f)
expectation (E.Rand (E.Constant n)) f =
  finiteExpectation [(1,E.constant i) | i <- [0..n-1]] (return . f)
expectation (E.Rand n) f = uniformExpectation (0,n-1) f
  -- logBlk "uniformExpectation" $ do
  -- logData "f" fv
  -- C.guarded (n .> 0) <$>
  --   case n of
  --     _ | not (v `S.member` C.variables fv) -> return fv
  --     _  -> C.divBy <$> boundedSum (v, fv) (0,n-1) <*> pure (fmap fromIntegral n)
  -- where
  --   v = E.Var "@i"
  --   fv = f (E.variable v)




-- Expectation Transformer
----------------------------------------------------------------------

extractRanking :: C -> BExp -> BExp -> CExp -> CExp -> SolveM [C.Norm]
extractRanking body i c g f = fmap templateToNorms $ do
  let gGNs = varGNorms g
      fGNs = varGNorms f
      -- fNorm = fNorms + fGrds
      -- gNorm = gNorms + gGrds
      gGrds = sum [ normFromBExp b | C.GNorm b _ _ <- gGNs]
      gNs    = sum [ norm n | C.GNorm _ _ n <- gGNs]
      gNorms = gNs + gGrds

      fGrds = sum [ normFromBExp b | C.GNorm b _ _ <- fGNs]
      fNs    = sum [ norm n | C.GNorm _ _ n <- fGNs]
      fNorms = fNs + fGrds

      grdNorm = normFromBExp i + normFromBExp c -- + gGrds
  df <- substituteM delta fNs
  let lin = normFromExp 1 + grdNorm + fNorms + gNorms + sum [ normFromBExp c * df ]
      -- cAdd = normFromExp 1 + grdNorm + gGrds
  template "linear" (pure lin)
    -- <|> template "shift-avg" (refine shiftAvg lin)
    -- <|> template "conditions" (refine conds lin)
    -- <|> template "shift-max" (refine shiftMax lin)
    -- <|> template "mixed" (pure (grdNorm * grdNorm + lin))
    <|> template "shift-avg" (refine shiftAvg lin)
    <|> template "conditions" (refine conds lin)
    <|> template "mixed-iteration" (pure (grdNorm * grdNorm + grdNorm))
    <|> template "shift-max" (refine shiftMax lin)
    <|> template "mixed-lin" (pure (grdNorm * lin + lin))
    <|> template "double mixed-iteration" (pure (grdNorm * grdNorm * grdNorm + grdNorm))
    -- <|> template "mixed-square" (pure (grdNorm * grdNorm + lin))
    <|> template "square-shift-max" ((\a -> lin * lin + a) <$> refine shiftMax lin)
   where
    refine m r = do
      r' <- m r
      if r' `normTemplateEq` r then empty else pure r'

    shiftAvg = substituteM s where
      s n@(C.Norm _ e) = do
        evn <- etM Evt body (C.nm 1 n)
        pure $ normFromExp $ E.maxE $ e : [2 * e - fmap floor e' | e' <- lfs evn ]

    shiftMax = substituteM s where
      s n@(C.Norm _ e) = do
        evn <- etM Evt body (C.nm 1 n)
        pure $ normFromExp $ E.maxE $ e : [2 * e - e' | C.GNorm _ _ (C.Norm _ e') <- varGNorms evn ]

    conds = substituteM s where
      s n = do
        evn <- etM Evt body (C.nm 1 n)
        pure $ norm n + sum [ normFromBExp b
                            | C.GNorm b _ _ <-  varGNorms evn ]

    delta n@(C.Norm _ e) = do
      ns <- S.toList . C.norms <$> etM Evt body (C.nm 1 n)
      let d = E.maxE [ ne | C.Norm _ ne <- ns ] - e
      return (normFromExp d)

    lfs (C.Sup c1 c2)    = lfs c1 ++ lfs c2
    lfs (C.Cond _ c1 c2) = lfs c1 ++ lfs c2
    lfs (C.Div d _)      = lfs d
    lfs (C.Plus c1 c2)   = [e1 + e2 | e1 <- lfs c1, e2 <- lfs c2]
    lfs (C.N k (C.Norm _ e)) = [E.constant k * fmap fromIntegral e]

isConstantWrt :: CExp -> C -> Bool
f `isConstantWrt` c = C.variables f `S.disjoint` vs where
  vs = S.fromList [ v | Ass v _ <- subPrograms c]


isTickFree :: C -> Bool
isTickFree = not . any isTick . subPrograms where
  isTick Tic{} = True
  isTick _     = False

isSimple :: C -> Bool
isSimple = not . any isWhile . subPrograms where
  isWhile While{} = True
  isWhile _       = False



solveInvariant :: UpperInvariant -> SolveM CExp
solveInvariant ui =
    maybe (throwError "no solution") return $
    solve (NaiveSolver 1 2) =<< debugM "invariant" ui



data Knd = Evt | Ect deriving (Eq, Ord, Show)

ect, evt :: C -> CExp -> SolveM CExp
ect c f =
  logBlk "Expected Cost" $
  logData "f" f >> logData "Program" c *> etM Ect c f

evt c f =
  logBlk "Expected Value" $
  logData "f" f >> logData "Program" c *> etM Evt c f


etM :: Knd -> C -> CExp -> SolveM CExp
-- etM k c f = choose (memoized et k c f)
etM k c f = (memoized et k c f)

et :: Knd -> C -> CExp -> SolveM CExp
et Ect c f
  | f == C.zero
    && isTickFree c        = return C.zero
et Evt c f
  | not (isSimple c)
    && f `isConstantWrt` c = return f
et Evt c (C.Plus f1 f2)
  | not (isSimple c)       = C.plus <$> evt c f1 <*> evt c f2
et _   Abort _             = return C.zero
et _   Skip  f             = return f
et Evt (Tic _) f           = return f
et Ect (Tic e) f           = return $ C.ramp e `C.plus` f
et _   (Ass v d) f         = expectation d (\ e -> E.substitute v e f)
et t (NonDet e1 e2) f      = C.sup <$> etM t e1 f <*> etM t e2 f
et t (Choice ls) f         = finiteExpectation ls (\ c -> etM t c f)
et t (Cond _ i b c1 c2) f  =  C.guarded i <$> (C.cond b <$> etM t c1 f <*> etM t c2 f)
et Ect (Seq c1 c2) f
  | isSimple c1            = et Ect c1 =<< et Ect c2 f
  | otherwise              = C.plus <$> ect c1 C.zero <*> (evt c1 =<< ect c2 f)
et t (Seq c1 c2) f         = etM t c1 =<< etM t c2 f
et t d@(While _  i b c) f  = logBlk "While.step" $ do
  logData "Problem" d
  logData "f" f
  g <- case t of {Evt -> return C.zero; Ect -> logBlk "Expected Cost Body" $ et Ect c C.zero}
  candidates <- logResult "Norms" $ extractRanking c i b g f
  let evnorm n = (Just <$> (n,) <$> evt c (C.fromNorm n)) `catchError` (\ _ -> return Nothing)
  (ns,hs) <- unzip <$> catMaybes <$> traverse evnorm candidates
--  hs <- traverse (et Evt c . C.fromNorm) ns
  let ui = UpperInvariant { inv  = i
                          , cnd = b
                          , cost = g
                          , step = hs
                          , cont = f
                          , limt = ns}
  logData "Invariant" ui
  solveInvariant ui
  -- h <- solveInvariant ui
  -- return (C.cond b h f)


-- * Pretty

instance PP.Pretty Knd where
  pretty = PP.text . show
