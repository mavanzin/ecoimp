{-# LANGUAGE ViewPatterns, RecordWildCards, DeriveFunctor #-}
module PWhile.InvariantSolver.Naive
  (
    NaiveSolver (..)
  , SmtExpression
  ) where

import qualified GHC.IO.Encoding as IOE
import qualified Data.Set as S
import           Control.Monad
import           Data.Maybe (isJust)
import           System.IO.Unsafe (unsafePerformIO)
import qualified Text.PrettyPrint.ANSI.Leijen as PP

import qualified GUBS.Algebra                 as SMT
import qualified GUBS.Polynomial              as P
import qualified GUBS.Solver                  as SMT
import qualified GUBS.Solver.Formula          as SMT

import qualified Data.PWhile.Expression as E
import qualified Data.PWhile.CostExpression as C
import           Data.PWhile.BoolExpression
import           PWhile.InvariantSolver

import           PWhile.Util

-- SMT monad
----------------------------------------------------------------------

type SmtM          = SMT.SolverM SMT.SMTLib2
type SmtFormula    = SMT.SMTFormula SMT.SMTLib2
type SmtExpression = SMT.SMTExpression SMT.SMTLib2

runSmtM :: SmtM a -> a
-- runSmtM = unsafePerformIO . SMT.runSMTLib2 "z3" ["-smt2", "-in"]
runSmtM m = unsafePerformIO $ IOE.setLocaleEncoding IOE.utf8 >> SMT.runSMTLib2Using "QF_LIRA" "yices-smt2" ["--incremental"] m
-- runSmtM = unsafePerformIO . SMT.runSMTLib2 "yices-smt2" []

-- MS: Optimisation with OptiMathSat (version 1.6.3)
-- there are two ways to process the output
--  1. the command '(get-objectives)'
--   returns bounds for all objective functions; the output is non-standard; example output;
--   (objectives
--   (x (- oo))
--   (y (- (/ 1 2)))
--   (z 0)
--   (t unsat)
--   ((+ y (to_real 1)) oo)
--   )
-- 2. the command (load-objective-model n) provides a (optimised) model wrt to n-th objective function
--   output is 'standard'; returns an error if n-th objective function has no model
--

-- MS: to undo the changes replace checkWithOptSolver with checkWithOptSolver

-- runSmtM = unsafePerformIO . SMT.runSMTLib2Using "QF_LIRA" "z3" ["-smt2", "-in"]
-- runSmtM = unsafePerformIO . SMT.runSMTLib2 "optimathsat" ["-model_generation=

minimize :: SmtExpression -> SmtM ()
minimize  e = SMT.sendExpressions "minimize" [e]

loadModel :: SmtM ()
loadModel   = SMT.sendCommand "(load-objective-model 0)"

framed :: SmtM a -> SmtM a
framed p = SMT.push *> p <* SMT.pop

freshReal :: SmtM SmtExpression
freshReal = P.variable <$> SMT.freshReal

bounded :: Maybe SmtExpression -> Maybe SmtExpression -> SmtM SmtExpression -> SmtM SmtExpression
bounded l u eM = do
  e <- eM
  maybe (pure ()) (SMT.assert . SMT.geqA e) l
  maybe (pure ()) (SMT.assert . (`SMT.geqA` e)) u
  return e


acoeff :: Rational -> SmtExpression
acoeff = P.coefficient . SMT.toRat

decSmtConst :: SmtExpression -> Maybe Rational
decSmtConst e = SMT.ratio <$> P.evalWithM (const Nothing) e

freshACoeff :: SmtM SmtExpression
freshACoeff = bounded (Just 0) Nothing freshReal


-- abstract polynomials
----------------------------------------------------------------------

type SmtPoly = P.Polynomial E.Var SmtExpression

instance {-# OVERLAPPING #-} PP.Pretty SmtPoly where
  pretty = P.ppPoly PP.pretty (PP.parens . PP.pretty)


withWeight :: SmtPoly -> SmtM SmtPoly
withWeight p = (*p) <$> do
  r <- bounded (Just 0) Nothing freshReal
  return (P.coefficient r)

fromExp :: E.Exp -> SmtPoly
fromExp = fmap SMT.num


-- abstract literals and formulas
----------------------------------------------------------------------

data SmtLit = Geq0 SmtPoly | TT | FF deriving (Eq, Ord, Show)

geqLit :: SmtPoly -> SmtPoly -> SmtLit
a `geqLit` b =
  case a - b of
    E.Constant (decSmtConst -> Just n1)
      | n1 >= 0 -> TT
      | otherwise -> FF
    e -> Geq0 e


fromBLit :: BLit -> SmtLit
fromBLit (e1 :>=: e2) = fromExp e1 `geqLit` fromExp e2

fromBExp :: BExp -> DNF SmtLit
fromBExp bexp = bigOr [ bigAnd [ literal (fromBLit l) | l <- lits c ]
                      | c <- conjs bexp]

instance Literal SmtLit where
  literal TT = Top
  literal FF = Bot
  literal l = lit l

  lneg TT = FF
  lneg FF = TT
  lneg (Geq0 a) = Geq0 (negate a - 1)

  (Geq0 a) `land` (Geq0 b) | (a + b) `geqLit` 0 == FF = Just (Left False)
  (Geq0 (E.AddConst a (E.Constant ka))) `land` (Geq0 (E.AddConst b (E.Constant kb)))
    | a == b = Just (Right (Geq0 (a + P.coefficient (P.coefficient (min ka kb)))))
  _ `land` _ = Nothing

-- cost expressions
----------------------------------------------------------------------

type CExp = C.CExp Rational
type AExp = C.CExp SmtExpression

instance C.FromRatio SmtExpression where
  fromRatio = P.coefficient . fromRational

decode :: AExp -> SmtM CExp
decode = C.traverseCoeff decodeCoeff where
  decodeCoeff = fmap SMT.ratio . P.evalWithM SMT.getValue

encode :: CExp -> AExp
encode = C.mapCoeff acoeff

-- invariant solver
----------------------------------------------------------------------

data NaiveSolver = NaiveSolver { degMin :: Int, degMax :: Int}

checkWithSolver :: NaiveSolver -> (Int -> SmtM a) -> (a -> SmtM b) -> SmtM (Maybe b)
checkWithSolver NaiveSolver{..} m mk = check degMin degMax where
  check i j | i > j = return Nothing
  check i j = do
    SMT.push
    a <- m i
    sat <- SMT.checkSat
    if sat then Just <$> mk a <* SMT.pop else SMT.pop >> check (i+1) j

-- MS: minimises (q0 + q1 + ... qn) of templates
checkWithOptSolver :: [SmtExpression] -> NaiveSolver -> (Int -> SmtM a) -> (a -> SmtM b) -> SmtM (Maybe b)
checkWithOptSolver qs NaiveSolver{..} m mk = check degMin degMax where
  check i j | i > j = return Nothing
  check i j = do
    SMT.push
    a <- m i
    minimize (sum qs)
    sat <- SMT.checkSat
    if sat
      then do
        loadModel
        Just <$> mk a <* SMT.pop
      else SMT.pop >> check (i+1) j


type Context = DNF SmtLit

data Constraint e = GEQ Context e e
  deriving (Functor)

eqCoefficientsWith :: ([SmtPoly] -> SmtM SmtPoly) -> Conj SmtLit -> SmtLit -> SmtM SmtFormula
eqCoefficientsWith _ _    TT = return SMT.Top
eqCoefficientsWith _ _    FF = return SMT.Bot
eqCoefficientsWith f cs (Geq0 b) =
  case toPolys (lits cs) of
    Nothing -> return SMT.Top
    Just ls -> SMT.smtAll (uncurry SMT.eqA) . P.zipCoefficients b <$> f ls
  where
    toPolys [] = Just []
    toPolys (FF:_) = Nothing
    toPolys (TT:ls) = toPolys ls
    toPolys (Geq0 p:ls) = (:) p <$> toPolys ls


handelman :: Int -> Conj SmtLit -> SmtLit -> SmtM SmtFormula
handelman deg = eqCoefficientsWith $
  \premise -> SMT.sumA <$> mapM withWeight [SMT.prod ms | ms <- replicateM deg (SMT.one : premise)]

assertEntailSmt :: Int -> DNF SmtLit -> SmtLit -> SmtM ()
assertEntailSmt i (Disj cs) p = forM_ cs assertEntailConj
  where
    assertEntailConj c
      | entailed  = return ()
      | otherwise = SMT.assert =<< handelman i c p
      where
        entailed = p == TT || p `S.member` litsSet c || any (`entailLit` p) c
    Geq0 eq `entailLit` Geq0 ep
      | E.Constant (E.Constant n) <- ep - eq = n >= 0
    _       `entailLit` _                    = False
    -- d = debug (show (PP.pretty (Disj cs) PP.<> PP.text " |- " PP.<> PP.pretty p))

assertConstraintSmt :: Int -> Constraint SmtPoly -> SmtM ()
assertConstraintSmt i (GEQ g l r) = assertEntailSmt i g (l `geqLit` r)

entailsSmtK :: PP.Pretty a => DNF SmtLit -> DNF SmtLit -> (Bool -> SmtM a) -> SmtM a
entailsSmtK p c k = anySatM (conjs c)
  where
    anySatM []       = k False
    anySatM (cs:css) = do
      SMT.push
      s <- allSatM (lits cs)
      if s then k True <* SMT.pop else SMT.pop >> anySatM css
    allSatM [] = return True
    allSatM ls = forM ls (ent p) *> SMT.checkSat

  -- TODO: add syntactic checks and possibly assert
    ent ctx c = assertEntailSmt 1 ctx c

-- elimination of cost expression
----------------------------------------------------------------------

toFrac :: C.Coeff c => C.CExp c -> (C.CExp c, E.Exp)
toFrac c@C.N{} = (c,1)
toFrac (C.Div c e) = (d,e*f) where
  (d,f) = toFrac c
toFrac (C.Sup c1 c2) = (C.scale f2 d1 `C.sup` C.scale f1 d2, f1 * f2 * f) where
  (d1,e1) = toFrac c1
  (d2,e2) = toFrac c2
  (f,(f1,f2)) = E.factor e1 e2
toFrac (C.Plus c1 c2) = (C.scale f2 d1 `C.plus` C.scale f1 d2, f1 * f2 * f) where
  (d1,e1) = toFrac c1
  (d2,e2) = toFrac c2
  (f,(f1,f2)) = E.factor e1 e2
toFrac (C.Cond g c1 c2) = (C.cond g (C.scale f2 d1) (C.scale f1 d2), f1 * f2 * f) where
  (d1,e1) = toFrac c1
  (d2,e2) = toFrac c2
  (f,(f1,f2)) = E.factor e1 e2

reduce :: Constraint AExp -> SmtM [Constraint SmtPoly]
reduce (GEQ c (toFrac -> (lhs,f1)) (toFrac -> (rhs,f2))) = do
  cs <- walkLhs lhs c (\ lhs' ctx' ->
                         walkRhs rhs ctx' (\ rhs' ctx'' ->
                                              return [GEQ ctx'' lhs' rhs']))
  return [GEQ g (fromExp f2' * l) (fromExp f1' * r) | GEQ g l r <- cs, r /= 0]
  where
    (_,(f1',f2')) = E.factor f1 f2

    branch (fromBExp -> g) m1 m2 ctx k =
      entailsSmtK ctx g $ \ entailed ->
      if entailed
      then m1 (ctx .&& g) k -- debugPA (PP.pretty ctx <> PP.text "=>" <> PP.pretty g) =<<
      else entailsSmtK ctx (neg g) $ \ negEntailed ->
        if negEntailed
        then m2 (ctx .&& neg g) k -- debugPA (PP.pretty ctx <> PP.text "=>" <> PP.pretty (neg' g)) =<<
        else (++) <$> m1 (ctx .&& g) k
             <*> m2 (ctx .&& neg g) k

    norm (C.Norm g e) =
      branch g (\ ctx' k' -> k' (fromExp e) ctx')
               (\ ctx' k' -> k' 0 ctx')

    add m1 m2 ctx k =
      m1 ctx $ \ e1 ctx' ->
      m2 ctx' $ \ e2 ->
      k (e1 + e2)

    scale l m ctx k =
      m ctx $ \ e -> k (P.coefficient l * e)

    walkLhs _                Bot _ = return []
    walkLhs (C.N c' n)       ctx k = scale c' (norm n) ctx k
    walkLhs (C.Cond g c1 c2) ctx k = branch g (walkLhs c1) (walkLhs c2) ctx k
    walkLhs (C.Plus c1 c2)   ctx k = add (walkLhs c1) (walkLhs c2) ctx k
    walkLhs C.Div {} _ _           = error "InvariantSolver.Naive.reduce: div on lhs"
    walkLhs C.Sup {} _ _           = error "InvariantSolver.Naive.reduce: sup on lhs"

    walkRhs _                Bot _ = return []
    walkRhs (C.N c' n)       ctx k = scale c' (norm n) ctx k
    walkRhs (C.Cond g c1 c2) ctx k = branch g (walkRhs c1) (walkRhs c2) ctx k
    walkRhs (C.Plus c1 c2)   ctx k = add (walkRhs c1) (walkRhs c2) ctx k
    walkRhs C.Div {} _ _           = error "InvariantSolver.Naive.reduce: div on rhs"
    walkRhs (C.Sup c1 c2)    ctx k =
      (++) <$> walkRhs c1 ctx k <*> walkRhs c2 ctx k

debug :: PP.Pretty e => String -> e -> e
debug = debugMsg "SmtSolver"

debugPA :: (Applicative f, PP.Pretty msg, PP.Pretty e) => msg -> e -> f e
debugPA = debugMsgA "SmtSolver" . renderPretty

upperInvariant :: NaiveSolver -> UpperInvariant -> SmtM (Maybe CExp)
upperInvariant s UpperInvariant{..} = do
  qs <- traverse (const freshACoeff) step
  let
    f = applyTemplate qs [ encode (C.fromNorm l) | l <- limt]
    g = C.cond cnd (encode cost `C.plus` applyTemplate qs (encode `map` step)) (encode cont)
    initial = debug "initial" (GEQ (fromBExp inv) f g)
  cs <- debug "final" <$> reduce initial
  checkWithSolver s
  -- checkWithOptSolver qs s
    (\ i -> assertConstraintSmt i `mapM_` cs)
    (\ _ -> C.guarded inv <$> decode f)
  where
    applyTemplate qs cs = C.sum (zipWith C.scaleC qs cs)


instance InvariantSolver NaiveSolver where
  entails s (fromBExp -> dnf) (fromBExp -> c) =
    case runSmtM $ checkWithSolver s (\ _ -> entailsSmtK dnf c return) return of
      Just True -> True
      _         -> False
  solve s ui = runSmtM $ upperInvariant s ui

-- pretty printers
----------------------------------------------------------------------

instance PP.Pretty SmtLit where
  pretty (Geq0 a) = PP.pretty a PP.<+> ppGeq PP.<+> PP.text "0"
  pretty TT = PP.text "TT"
  pretty FF = PP.text "FF"

instance (PP.Pretty e) => PP.Pretty (Constraint e) where
  pretty (GEQ p l r) = PP.group (PP.nest 2 ( PP.pretty p PP.<+> ppEntails PP.<$> ppIEQ))
    where ppIEQ = PP.group (PP.pretty l) PP.</> PP.text "≥" PP.<+> PP.group (PP.pretty r)
