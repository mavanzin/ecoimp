{-# LANGUAGE MultiParamTypeClasses, UndecidableInstances, CPP #-}
module PWhile.Util where

import qualified Control.Monad.Trace          as MTR
import qualified Data.Set                     as S
import           Data.Tree                    (Forest, Tree (..), drawTree)
import qualified Debug.Trace                  as TR
import qualified System.IO                    as IO
import qualified Text.PrettyPrint.ANSI.Leijen as PP


-- render pretty

renderPretty :: PP.Pretty e => e -> String
-- TODO
-- renderPretty d = ""
-- renderPretty d = PP.displayS (PP.renderCompact (PP.pretty d)) ""
renderPretty d = PP.displayS (PP.renderSmart 1.0 12000 (PP.pretty d)) ""

hPutPrettyLn :: PP.Pretty e => IO.Handle -> e -> IO ()
hPutPrettyLn h = IO.hPutStrLn h . renderPretty

putPrettyLn :: PP.Pretty e => e -> IO ()
putPrettyLn = hPutPrettyLn IO.stdout

putPrettyErrLn :: PP.Pretty e => e -> IO ()
putPrettyErrLn = hPutPrettyLn IO.stderr

-- pretty printers

ppCdot, ppAnd, ppOr, ppGeq, ppNot, ppAdd, ppLeq, ppImp, ppMul, ppSpace, ppEntails :: PP.Doc
ppCdot = PP.char '·'
ppAnd  = PP.char '∧'
ppOr   = PP.char '∨'
ppGeq  = PP.char '≥'
ppNot  = PP.char '¬'
ppAdd  = PP.char '+'
ppLeq  = PP.char '≼'
ppImp  = PP.text "==>"
ppMul  = PP.char '×'
ppSpace  = PP.char ' '
ppEntails = PP.char '⊨'

ppParen :: Bool -> PP.Doc -> PP.Doc
ppParen b d = if b then PP.parens d else d

-- log

renderLog :: Forest String -> PP.Doc
renderLog = PP.text . drawTree . Node "ExecutionLog"

putLog :: Forest String -> IO ()
putLog = putPrettyErrLn . renderLog

tracePretty :: PP.Pretty e => String -> e -> e
tracePretty s d = TR.trace (renderPretty (PP.hang 2 $ PP.group $ PP.fill 25 (PP.text s) PP.<$> PP.pretty d)) d

debugMsg :: PP.Pretty e => String -> String -> e -> e
#ifdef DEBUG
debugMsg s m = tracePretty ("[" ++ s ++ " " ++ m ++ "]")
#else
debugMsg _ _ = id
#endif

debugMsgA :: (Applicative f, PP.Pretty e) => String -> String -> e -> f e
debugMsgA s m = pure . debugMsg s m

logMsg :: MTR.MonadTrace String m => PP.Pretty e => e -> m ()
logMsg = MTR.trace . renderPretty

logMsg2 :: MTR.MonadTrace String m => (PP.Pretty a, PP.Pretty b) => a -> b -> m ()
logMsg2 a b = MTR.trace $ renderPretty $ PP.brackets (PP.pretty a) PP.<$$> PP.indent 2 (PP.pretty b)

logBlk :: MTR.MonadTrace String m => PP.Pretty e => e -> m a -> m a
logBlk = MTR.scopeTrace . renderPretty

-- set / list

nub :: Ord a => [a] -> [a]
nub = go S.empty where
  go _    []            = []
  go seen (x:xs)
    | x `S.member` seen = go seen xs
    | otherwise         = x: go (x `S.insert` seen) xs

union :: Ord a => [a] -> [a] -> [a]
union xs ys = xs ++ [ y | y <- ys, y `S.notMember` xs' ]
  where xs' = S.fromList xs

intersect :: Ord a => [a] -> [a] -> [a]
intersect xs ys = [ x | x <- xs, x `S.member` ys' ]
  where ys' = S.fromList ys

(\\) :: Ord a => [a] -> [a] -> [a]
xs \\ ys = [ x | x <- xs, x `S.notMember` ys' ]
  where ys' = S.fromList ys


-- misc

ifM :: Monad m => m Bool -> m a -> m a -> m a
ifM bM tt ff = bM >>= \b -> if b then tt else ff

fmap2 :: (Functor f1, Functor f2) => (a -> b) -> f2 (f1 a) -> f2 (f1 b)
fmap2 = fmap . fmap

fmap3 :: (Functor f1, Functor f2, Functor f3) => (a -> b) -> f3 (f2 (f1 a)) -> f3 (f2 (f1 b))
fmap3 = fmap . fmap . fmap
