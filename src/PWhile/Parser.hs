module PWhile.Parser
  ( fromFile
  , fromFile'
  , errorBundlePretty)
where

import           Data.Maybe (fromMaybe)
import           Control.Monad                  (void)
import           Data.Void

import           Control.Monad.Combinators.Expr
import           Text.Megaparsec
import           Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer     as L

import           Data.PWhile.Expression         as E
import           Data.PWhile.BoolExpression     as B
import           Data.PWhile.CostExpression     as C
import           PWhile.DSL                     as PW

-- indent-sensitive parser

-- parser
type Parser = Parsec Void String

parser :: Parser Program
parser = whitespace *> pDef <* eof

-- test :: FilePath -> IO()
-- test fp = do
--   i <- readFile fp
--   case runParser parser fp i of
--     Left  e -> putStrLn $ errorBundlePretty e
--     Right p -> print    $ pretty $ fst $ gen p

fromFile :: FilePath -> IO (Either (ParseErrorBundle String Void) Program)
fromFile fp = runParser parser fp <$> readFile fp

fromFile' :: FilePath -> IO Program
fromFile' fp = do
  pM <- fromFile fp
  case pM of
    Left  e -> error $ show $ errorBundlePretty e
    Right p -> return p


-- basic combinators

lineComment :: Parser ()
lineComment = L.skipLineComment "#"

whitespace,whitespaceN :: Parser ()
whitespaceN = L.space space1 lineComment empty
whitespace  = L.space spaceN lineComment empty where
  spaceN = void $ takeWhile1P Nothing (\c -> c == ' ' ||  c == '\t')

keywords :: [String]
keywords =
  [ "def", "while","skip","if","then","else","do"
  ,"and","or","not","abort","tick","var","prob","assume", "consume"
  ,"unif","rand","nondet","ber","bin", "hyper"]

keyword :: String -> Parser ()
keyword w = lexeme $ try $ string w *> notFollowedBy alphaNumChar

identifier :: Parser String
identifier = lexeme $ try $ do
  ident <- (:) <$> letterChar <*> many (alphaNumChar <|> char '_')
  if ident `elem` keywords
    then fail   $ "unexpected identifier: " ++ ident
    else return ident

lexeme :: Parser a -> Parser a
lexeme = L.lexeme whitespace

symbol :: String -> Parser String
symbol = L.symbol whitespace

parens,brackets :: Parser a -> Parser a
parens   = between (symbol "(") (symbol ")")
brackets = between (symbol "[") (symbol "]")

integer :: Parser Int
integer = lexeme (symbol "-" >> ((*) (-1) <$> L.decimal)) <|> lexeme L.decimal

comma, colon :: Parser ()
comma = void $ symbol ","
colon = void $ symbol ":"

withIndent :: (a -> [b] -> c) -> Parser a -> Parser b -> Parser c
withIndent cmb pHead pBody = L.indentBlock whitespaceN p where
  p = pHead >>= \a -> return $ L.IndentSome Nothing (return . cmb a) pBody


-- expression parser

pBExp :: Parser BExp
pBExp = makeExprParser bTerm bOperators where
  bTerm =
    try (parens pBExp)
    <|> ( do e1 <- pAExp
             op <-  ((.<=) <$ symbol "<=")
                    <|> ((.>=) <$ symbol "=>")
                    <|> ((.>=) <$ symbol ">=")
                    <|> ((.<)  <$ symbol "<")
                    <|> ((.>)  <$ symbol ">")
                    <|> ((.==)  <$ symbol "=")
             e2 <- pAExp
             return $ e1 `op` e2 )
  bOperators =
    [ [ Prefix (neg   <$ keyword "not") ]
    , [ InfixL ((.&&) <$ keyword "and")
      , InfixL ((.||) <$ keyword "or") ] ]

pAExp :: Parser Exp
pAExp = makeExprParser aTerm aOperators where
  aTerm
    = parens pAExp
    <|> (variable . Var) <$> identifier
    <|> pConstant
  aOperators =
    -- MA: check
    [ [ Prefix ((* (-1)) <$ symbol "-") ]
    , [ InfixL ((*) <$ symbol "*") ]
    , [ InfixL ((+) <$ symbol "+")
      , InfixN ((-) <$ symbol "-") ] ]

pConstant :: Parser Exp
pConstant = E.constant <$> integer

pGuard :: Parser BExp
pGuard = pBExp <* colon

pInvariant :: Parser (Maybe BExp)
pInvariant = optional $ brackets pBExp

pVar :: Parser Exp
pVar = (variable . Var) <$> identifier

-- command parser

pCmd :: Parser Program
pCmd
  =   pSkip
  <|> pAbort
  <|> pAssignment
  <|> pTick
  <|> pNondet
  <|> pAssume
  <|> pIte
  <|> pProb
  <|> pWhile

withBlock :: (a -> Program -> b) -> Parser a -> Parser b
withBlock cmb pHead = withIndent cmb' pHead pCmd where
  cmb' a cs = cmb a (sequ cs)
  sequ []     = skip
  sequ [x]    = x
  sequ (x:xs) = x >> sequ xs

withBlock' ::  Parser a -> Parser (a, Program)
withBlock' = withBlock (,)

pSkip :: Parser Program
pSkip = keyword "skip" *> pure skip

-- we ignore var declaration
pVars :: Parser Program
pVars = keyword "var" *> sepBy1 identifier (symbol ",") *> pure skip

pAbort :: Parser Program
pAbort = keyword "abort" *> pure abort

pTick :: Parser Program
pTick = keyword "tick" *> (consume <$> pAExp)

pAssignment :: Parser Program
pAssignment = do
  v <- pVar
  void $ symbol "="
  pDirac v <|> pRand v <|> pUnif v <|> pBer v <|> pBin v <|> pHyper v



pDirac :: Exp -> Parser Program
pDirac v = (v .=) <$> pAExp

pRand :: Exp -> Parser Program
pRand v = do
  e <- symbol "rand" *> parens pAExp
  return $ v .~ rand e

pUnif :: Exp -> Parser Program
pUnif v = do
  (a,b) <- symbol "unif" *> pTuple integer
  return $ (v .~) $ unif $ E.constant `fmap` [a..b]

pBer :: Exp -> Parser Program
pBer v = do
  (a,b) <- symbol "ber" *> pTuple integer
  return $ (v .~) $ bernoulli (fromIntegral a, fromIntegral $ b-a) 1 0

pHyper :: Exp -> Parser Program
pHyper v = do
  (a,b,c) <- symbol "hyper" *> pTriple integer
  return $ (v .~) $ hyper a b c

pBin :: Exp -> Parser Program
pBin v = do
  (a,b,c) <- symbol "bin" *> pTriple integer
  return $ (v .~) $ binomial (b,c) a

pTuple :: Parser a -> Parser (a,a)
pTuple p = parens $ p >>= \a -> comma >> p >>= \b -> return (a,b)

pTriple :: Parser a -> Parser (a,a,a)
pTriple p = parens $ p >>= \a -> comma >> p >>= \b -> comma >> p >>= \ c -> return (a,b,c)

pDef :: Parser Program
pDef =
  snd <$> withBlock' (keyword "def" *> identifier *> symbol "():" *> whitespaceN *> pVars)

pAssume :: Parser Program
pAssume = do
  g <- keyword "assume" *> pBExp
  return (ite' g skip abort)
  -- (g,tt) <- withBlock' (keyword "assume" *> pGuard)
  -- return $ assume g tt

pElse :: Parser Program
pElse = do
  elseM <- optional $ withBlock' (keyword "else" *> colon)
  return $ case elseM of
    Just ((),ff) -> ff
    _            -> skip

pIte :: Parser Program
pIte = do
  ((i,g) ,tt) <- withBlock' (keyword "if" *> ((,) <$> pInvariant <*> pGuard))
  ff      <- pElse
  return $ ite (Top `fromMaybe` i) g tt ff

pNondet :: Parser Program
pNondet = do
  tt <- withBlock (\_ p -> p) (keyword "nondet" *> colon)
  ff <- pElse
  return $ PW.nondet [tt,ff]

pProb :: Parser Program
pProb = do
  ((a,b),tt) <- withBlock' (keyword "prob" *> pTuple integer <* colon)
  ff         <- pElse
  return $ PW.choice [ (E.constant a, tt), (E.constant b, ff) ]

pWhile :: Parser Program
pWhile = do
  ((i,g),cs) <- withBlock' (keyword "while" *> ((,) <$> pInvariant <*> pGuard))
  return $ while (Top `fromMaybe` i) g cs
