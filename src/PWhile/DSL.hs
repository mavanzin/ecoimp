{-# LANGUAGE TupleSections #-}
module PWhile.DSL
  (
    module Data.PWhile.Expression
  , module Data.PWhile.BoolExpression
  , Program, gen
  , (.=), (.~)
  , var, vars
  , skip, abort, while, consume, tick, ite, choice, nondet, (.<|>)
  , while', ite', assume
  , body, hd, tl
  )
where

import           Control.Arrow (second)
import           Control.Monad.State
import           Control.Monad.Writer
import qualified Data.Set as S

import           PWhile.Util
import           Data.PWhile.Program
import           Data.PWhile.Expression
import qualified Data.PWhile.Expression as E
import           Data.PWhile.BoolExpression hiding (variables)

-- generator monad
----------------------------------------------------------------------

newtype Gen a = Gen {unGen :: WriterT (C, VarList) (State Int) a}
  deriving (MonadWriter (C,VarList), MonadState Int, Applicative, Functor, Monad)


newtype VarList = VarList [Var]

instance Semigroup VarList where
  VarList l1 <> VarList l2 = VarList (l1 `union` l2)

instance Monoid VarList where
  mempty                            = VarList []


type Program = Gen ()

instance Show Program where
  show = renderPretty . fst . gen

label :: Gen Label
label = get <* modify succ

-- gensym :: Gen Var
-- gensym = (v <$> get) <* modify succ where v i = Named ("x@" ++ show i)

cmd :: C -> Gen ()
cmd c = tell (c,mempty)

getCmd :: Program -> Gen C
getCmd p = Gen (lift (fst <$> execWriterT (unGen p)))

var :: String -> Gen Exp
var n = tell (mempty, VarList [Var n]) >> return (variable (Var n))

vars :: [String] -> Gen [Exp]
vars = mapM var

gen :: Program -> (C, [Var])
gen p = second (\(VarList l) -> l) $ evalState (execWriterT (unGen p)) 0

-- pprint :: PP.Pretty e => e -> IO ()
-- pprint e = PP.displayIO stdout (PP.renderSmart 0.8 120 (PP.pretty e PP.<$> PP.empty))


-- sugar
----------------------------------------------------------------------

infixl 1 .=

(.=) :: Exp -> Exp -> Program
v .= e = v .~ dirac e

infixl 1 .~
(.~) :: Exp -> Dist Exp -> Program
v .~ d = case S.toList (E.variables v) of
           [x] -> cmd (Ass x d)
           _ -> error "(.~) unexpected case"

skip :: Program
skip = cmd Skip

abort :: Program
abort = cmd Abort

while :: Invariant -> BExp -> Program -> Program
while i g p = cmd =<< While <$> label <*> pure i <*> pure g <*> getCmd p

consume :: Exp -> Program
consume = cmd . Tic

tick :: Program
tick = consume (constant 1)

ite :: Invariant -> BExp -> Program -> Program -> Program
ite i g p1 p2 = cmd =<< Cond <$> label <*> pure i <*> pure g <*> getCmd p1 <*> getCmd p2

choice :: [(Exp, Program)] -> Program
choice cs = cmd =<< Choice <$> sequence [(w,) <$> getCmd p | (w,p) <- cs]


nondet :: [Program] -> Program
nondet [] = skip
nondet [p] = p
nondet (p:qs) = p .<|> nondet qs

infixl 3 .<|>
(.<|>) :: Program -> Program -> Program
p .<|> q  = cmd =<< NonDet <$> getCmd p <*> getCmd q


-- more sugar
----------------------------------------------------------------------

while' :: BExp -> Program -> Program
while' = while top

ite' :: BExp -> Program -> Program -> Program
ite' = ite top

assume :: BExp -> Program -> Program
assume g p = do
  ite' g skip abort;
  p

-- deconstructors
----------------------------------------------------------------------

body :: Program -> Program
body p = getCmd p >>= bdy where
  bdy (While _ _ _ c) = cmd c
  bdy _               = error "DSL.body: not a while loop"

hd,tl :: Program -> Program
hd p = getCmd p >>= first' where
  first' (Seq c _) = cmd c
  first' (Seq _ _) = error "DSL.first: not a sequence"
tl p = getCmd p >>= second' where
  second' (Seq _ c) = cmd c
  second' (Seq _ _) = error "DSL.second: not a sequence"
